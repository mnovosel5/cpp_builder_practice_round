
// ************************************************************** //
//                                                              
//                       XML Data Binding                       
//                                                              
//         Generated on: 3/16/2019 3:58:11 PM                   
//       Generated from: C:\Users\mnovosel2\Desktop\books.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\books.xdb   
//                                                              
// ************************************************************** //

#ifndef   phonesH
#define   phonesH

#include <System.hpp>
#include <Xml.Xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.XMLDoc.hpp>
#include <XMLNodeImp.h>
#include <Xml.xmlutil.hpp>


// Forward Decls

__interface IXMLphonebookType;
typedef System::DelphiInterface<IXMLphonebookType> _di_IXMLphonebookType;
__interface IXMLcontactType;
typedef System::DelphiInterface<IXMLcontactType> _di_IXMLcontactType;

// IXMLphonebookType 

__interface INTERFACE_UUID("{F36A52FC-0593-4C48-93DB-C773F383C307}") IXMLphonebookType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual _di_IXMLcontactType __fastcall Get_contact(int Index) = 0;
  // Methods & Properties 
  virtual _di_IXMLcontactType __fastcall Add() = 0;
  virtual _di_IXMLcontactType __fastcall Insert(const int Index) = 0;
  __property _di_IXMLcontactType contact[int Index] = { read=Get_contact };/* default */
};

// IXMLcontactType 

__interface INTERFACE_UUID("{CC68E304-FD59-4D18-A1DF-30D6B991CF6A}") IXMLcontactType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_name() = 0;
  virtual System::UnicodeString __fastcall Get_surname() = 0;
  virtual System::UnicodeString __fastcall Get_address() = 0;
  virtual System::UnicodeString __fastcall Get_phone() = 0;
  virtual void __fastcall Set_name(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_surname(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_address(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_phone(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString name = { read=Get_name, write=Set_name };
  __property System::UnicodeString surname = { read=Get_surname, write=Set_surname };
  __property System::UnicodeString address = { read=Get_address, write=Set_address };
  __property System::UnicodeString phone = { read=Get_phone, write=Set_phone };
};

// Forward Decls 

class TXMLphonebookType;
class TXMLcontactType;

// TXMLphonebookType 

class TXMLphonebookType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLphonebookType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLphonebookType 
  virtual _di_IXMLcontactType __fastcall Get_contact(int Index);
  virtual _di_IXMLcontactType __fastcall Add();
  virtual _di_IXMLcontactType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLcontactType 

class TXMLcontactType : public Xml::Xmldoc::TXMLNode, public IXMLcontactType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLcontactType 
  virtual System::UnicodeString __fastcall Get_name();
  virtual System::UnicodeString __fastcall Get_surname();
  virtual System::UnicodeString __fastcall Get_address();
  virtual System::UnicodeString __fastcall Get_phone();
  virtual void __fastcall Set_name(System::UnicodeString Value);
  virtual void __fastcall Set_surname(System::UnicodeString Value);
  virtual void __fastcall Set_address(System::UnicodeString Value);
  virtual void __fastcall Set_phone(System::UnicodeString Value);
};

// Global Functions 

_di_IXMLphonebookType __fastcall Getphonebook(Xml::Xmlintf::_di_IXMLDocument Doc);
_di_IXMLphonebookType __fastcall Getphonebook(Xml::Xmldoc::TXMLDocument *Doc);
_di_IXMLphonebookType __fastcall Loadphonebook(const System::UnicodeString& FileName);
_di_IXMLphonebookType __fastcall  Newphonebook();

#define TargetNamespace ""

#endif