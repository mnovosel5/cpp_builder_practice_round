#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "phones.h"
#include <registry.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {

}

void __fastcall Tmain_form::load_buttonClick(TObject *Sender) {
	_di_IXMLphonebookType PhoneBook = Getphonebook(xml_document);
	list_view->Items->Clear();
	for(int i = 0; i < PhoneBook->Count; i++) {
		list_view->Items->Add();
		list_view->Items->Item[i]->Caption = PhoneBook->contact[i]->name;
		list_view->Items->Item[i]->SubItems->Add(PhoneBook->contact[i]->surname);
		list_view->Items->Item[i]->SubItems->Add(PhoneBook->contact[i]->address);
		list_view->Items->Item[i]->SubItems->Add(PhoneBook->contact[i]->phone);
	}
}

void __fastcall Tmain_form::add_buttonClick(TObject *Sender) {
	_di_IXMLphonebookType PhoneBook = Getphonebook(xml_document);
	_di_IXMLcontactType Contact = PhoneBook->Add();
	Contact->name = name_edit->Text;
	Contact->surname = surname_edit->Text;
	Contact->address = address_edit->Text;
	Contact->phone = phone_edit->Text;
	xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::delete_buttonClick(TObject *Sender) {
	_di_IXMLphonebookType PhoneBook = Getphonebook(xml_document);
	PhoneBook->Delete(list_view->ItemIndex);
	xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::update_buttonClick(TObject *Sender) {
	_di_IXMLphonebookType PhoneBook = Getphonebook(xml_document);
    _di_IXMLcontactType Contact = PhoneBook->contact[list_view->ItemIndex];
    Contact->name = name_edit->Text;
	Contact->surname = surname_edit->Text;
    Contact->address = address_edit->Text;
    Contact->phone = phone_edit->Text;
	xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::FormClose(TObject *Sender, TCloseAction &Action) {
    /*

        TIniFile *ini = new TIniFile(GetCurrentDir() + "\\settings.ini");
        ini->WriteInteger("Main Window", "Left", Left);
        ini->WriteInteger("Main Window", "Top", Top);
        ini->WriteInteger("Main Window", "Width", Width);
        ini->WriteInteger("Main Window", "Height", Height);
        delete ini;

    */
    // Saving in the registry
	TRegistry *registry = new TRegistry;
    registry->RootKey = HKEY_LOCAL_MACHINE;
    UnicodeString key = "Software\\Matija";
    // Create a key
    registry->CreateKey(key);
    if(registry->OpenKey(key, true)) {
        registry->WriteInteger("Left", Left);
        registry->WriteInteger("Top", Top);
        registry->WriteInteger("Width", Width);
        registry->WriteInteger("Height", Height);
        registry->CloseKey();
	}
	delete registry;
}

void __fastcall Tmain_form::FormCreate(TObject *Sender) {
    /*

        TIniFile *ini = new TIniFile(GetCurrentDir() + "\\settings.ini");
        Top = ini->ReadInteger("Main Window", "Left", 0);
        Left = ini->ReadInteger("Main Window", "Top", 0);
        Width = ini->ReadInteger("Main Window", "Width", 565);
        Height = ini->ReadInteger("Main Window", "Height", 439);
        delete ini;

    */
    // Loading the settings from the registry
	TRegistry *registry = new TRegistry;
    registry->RootKey = HKEY_LOCAL_MACHINE;
    UnicodeString key = "Software\\Matija";
    // Create a key
    if(registry->KeyExists(key)) {
        if(registry->OpenKey(key, true)) {
            Left = registry->ReadInteger("Left");
            Top = registry->ReadInteger("Top");
            Width = registry->ReadInteger("Width");
            Height = registry->ReadInteger("Height");
            registry->CloseKey();
        }
	}
	delete registry;
}

