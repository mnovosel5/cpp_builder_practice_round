object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'XML testing'
  ClientHeight = 400
  ClientWidth = 549
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    549
    400)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 40
    Width = 120
    Height = 24
    Caption = 'phone_book'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 390
    Top = 40
    Width = 132
    Height = 24
    Caption = 'input_panel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object list_view: TListView
    Left = 8
    Top = 70
    Width = 376
    Height = 283
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvRaised
    BevelKind = bkSoft
    BiDiMode = bdLeftToRight
    Color = cl3DLight
    Columns = <
      item
        Caption = 'name'
      end
      item
        Caption = 'surname'
      end
      item
        Caption = 'address'
      end
      item
        Caption = 'phone'
      end>
    ColumnClick = False
    ParentBiDiMode = False
    TabOrder = 0
    ViewStyle = vsReport
  end
  object load_button: TButton
    Left = 8
    Top = 367
    Width = 75
    Height = 25
    Caption = 'Refresh XML'
    TabOrder = 1
    OnClick = load_buttonClick
  end
  object add_button: TButton
    Left = 89
    Top = 367
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = add_buttonClick
  end
  object delete_button: TButton
    Left = 170
    Top = 367
    Width = 95
    Height = 25
    Caption = 'Delete selected'
    TabOrder = 3
    OnClick = delete_buttonClick
  end
  object Panel1: TPanel
    Left = 390
    Top = 70
    Width = 147
    Height = 211
    TabOrder = 4
    object Label2: TLabel
      Left = 8
      Top = 13
      Width = 28
      Height = 14
      Caption = 'Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 60
      Width = 49
      Height = 14
      Caption = 'Surname'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 107
      Width = 49
      Height = 14
      Caption = 'Address'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 8
      Top = 154
      Width = 35
      Height = 14
      Caption = 'Phone'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
    end
    object name_edit: TEdit
      Left = 8
      Top = 33
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object surname_edit: TEdit
      Left = 8
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object address_edit: TEdit
      Left = 8
      Top = 127
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object phone_edit: TEdit
      Left = 8
      Top = 174
      Width = 121
      Height = 21
      TabOrder = 3
    end
  end
  object update_button: TButton
    Left = 271
    Top = 367
    Width = 90
    Height = 25
    Caption = 'Update selected'
    TabOrder = 5
    OnClick = update_buttonClick
  end
  object xml_document: TXMLDocument
    FileName = 'C:\Users\mnovosel2\Desktop\phones.xml'
    Left = 200
    Top = 16
  end
end
