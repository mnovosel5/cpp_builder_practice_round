//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TListView *list_view;
	TXMLDocument *xml_document;
	TButton *load_button;
	TLabel *Label1;
	TButton *add_button;
	TButton *delete_button;
	TPanel *Panel1;
	TLabel *Label3;
	TEdit *name_edit;
	TEdit *surname_edit;
	TLabel *Label2;
	TLabel *Label4;
	TLabel *Label5;
	TEdit *address_edit;
	TLabel *Label6;
	TEdit *phone_edit;
	TButton *update_button;
	void __fastcall load_buttonClick(TObject *Sender);
	void __fastcall add_buttonClick(TObject *Sender);
	void __fastcall delete_buttonClick(TObject *Sender);
	void __fastcall update_buttonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
