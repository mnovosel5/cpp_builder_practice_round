object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'vjezba_2'
  ClientHeight = 299
  ClientWidth = 422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object input_panel: TPanel
    Left = 229
    Top = 8
    Width = 185
    Height = 249
    TabOrder = 0
    object Label1: TLabel
      Left = 80
      Top = 8
      Width = 38
      Height = 13
      Caption = 'Contact'
    end
    object Label2: TLabel
      Left = 8
      Top = 39
      Width = 27
      Height = 13
      Caption = 'Name'
    end
    object Label3: TLabel
      Left = 8
      Top = 85
      Width = 42
      Height = 13
      Caption = 'Surname'
    end
    object Label4: TLabel
      Left = 8
      Top = 131
      Width = 50
      Height = 13
      Caption = 'Telephone'
    end
    object Shape1: TShape
      Left = 8
      Top = 25
      Width = 169
      Height = 2
      Brush.Color = clBackground
    end
    object name_edit: TEdit
      Left = 8
      Top = 58
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object surname_edit: TEdit
      Left = 8
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object telephone_edit: TEdit
      Left = 8
      Top = 150
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object list_box: TListBox
    Left = 8
    Top = 8
    Width = 215
    Height = 249
    ItemHeight = 13
    TabOrder = 1
  end
  object load_button: TButton
    Left = 8
    Top = 266
    Width = 75
    Height = 25
    Caption = 'Load XML'
    TabOrder = 2
    OnClick = load_buttonClick
  end
  object edit_button: TButton
    Left = 89
    Top = 266
    Width = 88
    Height = 25
    Caption = 'Edit selected'
    TabOrder = 3
    OnClick = edit_buttonClick
  end
  object add_button: TButton
    Left = 183
    Top = 266
    Width = 75
    Height = 25
    Caption = 'Add new'
    TabOrder = 4
    OnClick = add_buttonClick
  end
  object save_button: TButton
    Left = 264
    Top = 266
    Width = 75
    Height = 25
    Caption = 'Save selected'
    TabOrder = 5
    OnClick = save_buttonClick
  end
  object xml_document: TXMLDocument
    FileName = 
      'C:\Users\mnovosel2\Documents\Embarcadero\Studio\Projects\contact' +
      's.xml'
    Left = 104
    Top = 112
  end
end
