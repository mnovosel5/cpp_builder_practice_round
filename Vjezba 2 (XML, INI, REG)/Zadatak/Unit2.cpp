#include <vcl.h>
#pragma hdrstop

#include "Unit2.h"
#include "contacts.h"
#include <StrUtils.hpp>
#include <Registry.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"

Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {

}

void __fastcall Tmain_form::load_buttonClick(TObject *Sender) {
    list_box->Clear();
    _di_IXMLphonebookType phone_book = Getphonebook(xml_document);
    for(int i = 0; i < phone_book->Count; i++) {
        list_box->Items->Add(
			phone_book->contact[i]->name
			+ " " + phone_book->contact[i]->surname
			+ " " + phone_book->contact[i]->phone
		);
	}
}

void __fastcall Tmain_form::edit_buttonClick(TObject *Sender) {
    String information = list_box->Items->Strings[list_box->ItemIndex];
    TStringDynArray explode = Strutils::SplitString(information, " ");
    name_edit->Text = explode[0];
    surname_edit->Text = explode[1];
    telephone_edit->Text = explode[2];
}

void __fastcall Tmain_form::add_buttonClick(TObject *Sender) {
    _di_IXMLphonebookType phone_book = Getphonebook(xml_document);
    _di_IXMLcontactType contact = phone_book->Add();
    contact->name = name_edit->Text;
    contact->surname = surname_edit->Text;
    contact->phone = telephone_edit->Text;
    xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::save_buttonClick(TObject *Sender) {
    _di_IXMLphonebookType phone_book = Getphonebook(xml_document);
    _di_IXMLcontactType contact = phone_book->contact[list_box->ItemIndex];
    contact->name = name_edit->Text;
    contact->surname = surname_edit->Text;
    contact->phone = telephone_edit->Text;
    xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::FormClose(TObject *Sender, TCloseAction &Action) {
    /*

        TIniFile *ini = new TIniFile(GetCurrentDir() + "\\settings.ini");
        ini->WriteInteger("Main Window", "Left", Left);
        ini->WriteInteger("Main Window", "Top", Top);
        ini->WriteInteger("Main Window", "Width", Width);
        ini->WriteInteger("Main Window", "Height", Height);
        delete ini;

    */
    // Saving in the registry
	TRegistry *registry = new TRegistry;
    registry->RootKey = HKEY_LOCAL_MACHINE;
    UnicodeString key = "Software\\Matija";
    // Create a key
    registry->CreateKey(key);
    if(registry->OpenKey(key, true)) {
        registry->WriteInteger("Left", Left);
        registry->WriteInteger("Top", Top);
        registry->CloseKey();
	}
	delete registry;
}

void __fastcall Tmain_form::FormCreate(TObject *Sender) {
    /*

        TIniFile *ini = new TIniFile(GetCurrentDir() + "\\settings.ini");
        Top = ini->ReadInteger("Main Window", "Left", 0);
        Left = ini->ReadInteger("Main Window", "Top", 0);
        Width = ini->ReadInteger("Main Window", "Width", 565);
        Height = ini->ReadInteger("Main Window", "Height", 439);
        delete ini;

    */
    // Loading the settings from the registry
	TRegistry *registry = new TRegistry;
    registry->RootKey = HKEY_LOCAL_MACHINE;
    UnicodeString key = "Software\\Matija";
    // Create a key
    if(registry->KeyExists(key)) {
        if(registry->OpenKey(key, true)) {
            Left = registry->ReadInteger("Left");
            Top = registry->ReadInteger("Top");
            registry->CloseKey();
        }
	}
	delete registry;
}
