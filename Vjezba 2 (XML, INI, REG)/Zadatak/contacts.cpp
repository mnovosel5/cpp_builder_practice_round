
// *********************************************************************************************** //
//                                                                                               
//                                       XML Data Binding                                        
//                                                                                               
//         Generated on: 3/16/2019 8:26:54 PM                                                    
//       Generated from: C:\Users\mnovosel2\Documents\Embarcadero\Studio\Projects\contacts.xml   
//   Settings stored in: C:\Users\mnovosel2\Documents\Embarcadero\Studio\Projects\contacts.xdb   
//                                                                                               
// *********************************************************************************************** //

#include <System.hpp>
#pragma hdrstop

#include "contacts.h"


// Global Functions 

_di_IXMLphonebookType __fastcall Getphonebook(Xml::Xmlintf::_di_IXMLDocument Doc)
{
  return (_di_IXMLphonebookType) Doc->GetDocBinding("phonebook", __classid(TXMLphonebookType), TargetNamespace);
};

_di_IXMLphonebookType __fastcall Getphonebook(Xml::Xmldoc::TXMLDocument *Doc)
{
  Xml::Xmlintf::_di_IXMLDocument DocIntf;
  Doc->GetInterface(DocIntf);
  return Getphonebook(DocIntf);
};

_di_IXMLphonebookType __fastcall Loadphonebook(const System::UnicodeString& FileName)
{
  return (_di_IXMLphonebookType) Xml::Xmldoc::LoadXMLDocument(FileName)->GetDocBinding("phonebook", __classid(TXMLphonebookType), TargetNamespace);
};

_di_IXMLphonebookType __fastcall  Newphonebook()
{
  return (_di_IXMLphonebookType) Xml::Xmldoc::NewXMLDocument()->GetDocBinding("phonebook", __classid(TXMLphonebookType), TargetNamespace);
};

// TXMLphonebookType 

void __fastcall TXMLphonebookType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("contact"), __classid(TXMLcontactType));
  ItemTag = "contact";
  ItemInterface = __uuidof(IXMLcontactType);
  Xml::Xmldoc::TXMLNodeCollection::AfterConstruction();
};

_di_IXMLcontactType __fastcall TXMLphonebookType::Get_contact(int Index)
{
  return (_di_IXMLcontactType) List->Nodes[Index];
};

_di_IXMLcontactType __fastcall TXMLphonebookType::Add()
{
  return (_di_IXMLcontactType) AddItem(-1);
};

_di_IXMLcontactType __fastcall TXMLphonebookType::Insert(const int Index)
{
  return (_di_IXMLcontactType) AddItem(Index);
};

// TXMLcontactType 

System::UnicodeString __fastcall TXMLcontactType::Get_name()
{
  return GetChildNodes()->Nodes[System::UnicodeString("name")]->Text;
};

void __fastcall TXMLcontactType::Set_name(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("name")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLcontactType::Get_surname()
{
  return GetChildNodes()->Nodes[System::UnicodeString("surname")]->Text;
};

void __fastcall TXMLcontactType::Set_surname(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("surname")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLcontactType::Get_phone()
{
  return GetChildNodes()->Nodes[System::UnicodeString("phone")]->Text;
};

void __fastcall TXMLcontactType::Set_phone(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("phone")]->NodeValue = Value;
};

// TXMLcontactTypeList 

_di_IXMLcontactType __fastcall TXMLcontactTypeList::Add()
{
  return (_di_IXMLcontactType) AddItem(-1);
};

_di_IXMLcontactType __fastcall TXMLcontactTypeList::Insert(const int Index)
{
  return (_di_IXMLcontactType) AddItem(Index);
};

_di_IXMLcontactType __fastcall TXMLcontactTypeList::Get_Item(int Index)
{
  return (_di_IXMLcontactType) List->Nodes[Index];
};
