
// *********************************************************************************************** //
//                                                                                               
//                                       XML Data Binding                                        
//                                                                                               
//         Generated on: 3/16/2019 8:26:54 PM                                                    
//       Generated from: C:\Users\mnovosel2\Documents\Embarcadero\Studio\Projects\contacts.xml   
//   Settings stored in: C:\Users\mnovosel2\Documents\Embarcadero\Studio\Projects\contacts.xdb   
//                                                                                               
// *********************************************************************************************** //

#ifndef   contactsH
#define   contactsH

#include <System.hpp>
#include <Xml.Xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.XMLDoc.hpp>
#include <XMLNodeImp.h>
#include <Xml.xmlutil.hpp>


// Forward Decls 

__interface IXMLphonebookType;
typedef System::DelphiInterface<IXMLphonebookType> _di_IXMLphonebookType;
__interface IXMLcontactType;
typedef System::DelphiInterface<IXMLcontactType> _di_IXMLcontactType;
__interface IXMLcontactTypeList;
typedef System::DelphiInterface<IXMLcontactTypeList> _di_IXMLcontactTypeList;

// IXMLphonebookType 

__interface INTERFACE_UUID("{F0ECF9C4-6DAB-4C79-BF7A-1379A74A85C7}") IXMLphonebookType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual _di_IXMLcontactType __fastcall Get_contact(int Index) = 0;
  // Methods & Properties 
  virtual _di_IXMLcontactType __fastcall Add() = 0;
  virtual _di_IXMLcontactType __fastcall Insert(const int Index) = 0;
  __property _di_IXMLcontactType contact[int Index] = { read=Get_contact };/* default */
};

// IXMLcontactType 

__interface INTERFACE_UUID("{1D863AA4-D207-42D9-B66E-7DB01FA4A06A}") IXMLcontactType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_name() = 0;
  virtual System::UnicodeString __fastcall Get_surname() = 0;
  virtual System::UnicodeString __fastcall Get_phone() = 0;
  virtual void __fastcall Set_name(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_surname(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_phone(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString name = { read=Get_name, write=Set_name };
  __property System::UnicodeString surname = { read=Get_surname, write=Set_surname };
  __property System::UnicodeString phone = { read=Get_phone, write=Set_phone };
};

// IXMLcontactTypeList 

__interface INTERFACE_UUID("{5F29EEDC-A9A7-4BE7-97B3-98D5C5E41C12}") IXMLcontactTypeList : public Xml::Xmlintf::IXMLNodeCollection
{
public:
  // Methods & Properties 
  virtual _di_IXMLcontactType __fastcall Add() = 0;
  virtual _di_IXMLcontactType __fastcall Insert(const int Index) = 0;

  virtual _di_IXMLcontactType __fastcall Get_Item(int Index) = 0;
  __property _di_IXMLcontactType Items[int Index] = { read=Get_Item /* default */ };
};

// Forward Decls 

class TXMLphonebookType;
class TXMLcontactType;
class TXMLcontactTypeList;

// TXMLphonebookType 

class TXMLphonebookType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLphonebookType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLphonebookType 
  virtual _di_IXMLcontactType __fastcall Get_contact(int Index);
  virtual _di_IXMLcontactType __fastcall Add();
  virtual _di_IXMLcontactType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLcontactType 

class TXMLcontactType : public Xml::Xmldoc::TXMLNode, public IXMLcontactType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLcontactType 
  virtual System::UnicodeString __fastcall Get_name();
  virtual System::UnicodeString __fastcall Get_surname();
  virtual System::UnicodeString __fastcall Get_phone();
  virtual void __fastcall Set_name(System::UnicodeString Value);
  virtual void __fastcall Set_surname(System::UnicodeString Value);
  virtual void __fastcall Set_phone(System::UnicodeString Value);
};

// TXMLcontactTypeList 

class TXMLcontactTypeList : public Xml::Xmldoc::TXMLNodeCollection, public IXMLcontactTypeList
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLcontactTypeList 
  virtual _di_IXMLcontactType __fastcall Add();
  virtual _di_IXMLcontactType __fastcall Insert(const int Index);

  virtual _di_IXMLcontactType __fastcall Get_Item(int Index);
};

// Global Functions 

_di_IXMLphonebookType __fastcall Getphonebook(Xml::Xmlintf::_di_IXMLDocument Doc);
_di_IXMLphonebookType __fastcall Getphonebook(Xml::Xmldoc::TXMLDocument *Doc);
_di_IXMLphonebookType __fastcall Loadphonebook(const System::UnicodeString& FileName);
_di_IXMLphonebookType __fastcall  Newphonebook();

#define TargetNamespace ""

#endif