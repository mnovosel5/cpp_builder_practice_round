//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TPanel *input_panel;
	TListBox *list_box;
	TButton *load_button;
	TButton *edit_button;
	TButton *add_button;
	TButton *save_button;
	TLabel *Label1;
	TEdit *name_edit;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *surname_edit;
	TLabel *Label4;
	TEdit *telephone_edit;
	TShape *Shape1;
	TXMLDocument *xml_document;
	void __fastcall load_buttonClick(TObject *Sender);
	void __fastcall edit_buttonClick(TObject *Sender);
	void __fastcall add_buttonClick(TObject *Sender);
	void __fastcall save_buttonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
