object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'Entry'
  ClientHeight = 215
  ClientWidth = 202
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object data_group: TGroupBox
    Left = 8
    Top = 8
    Width = 185
    Height = 169
    Caption = 'Data'
    TabOrder = 0
    object name_edit: TLabeledEdit
      Left = 11
      Top = 40
      Width = 121
      Height = 21
      EditLabel.Width = 27
      EditLabel.Height = 13
      EditLabel.Caption = 'Name'
      TabOrder = 0
    end
    object surname_edit: TLabeledEdit
      Left = 11
      Top = 80
      Width = 121
      Height = 21
      EditLabel.Width = 42
      EditLabel.Height = 13
      EditLabel.Caption = 'Surname'
      TabOrder = 1
    end
    object agree_check_box: TCheckBox
      Left = 11
      Top = 107
      Width = 97
      Height = 17
      Caption = 'I agree'
      TabOrder = 2
    end
    object enter_button: TButton
      Left = 57
      Top = 130
      Width = 75
      Height = 25
      Caption = 'Enter'
      TabOrder = 3
    end
  end
  object Button1: TButton
    Left = 8
    Top = 183
    Width = 75
    Height = 25
    Caption = 'Croatian'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 119
    Top = 183
    Width = 75
    Height = 25
    Caption = 'English'
    TabOrder = 2
    OnClick = Button2Click
  end
end
