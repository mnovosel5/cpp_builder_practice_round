#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"
#include "reinit.hpp"

#pragma package(smart_init)
#pragma resource "*.dfm"

Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {

}

void __fastcall Tmain_form::Button1Click(TObject *Sender) {
    const int CROATIAN = (SUBLANG_CROATIAN_CROATIA << 10) | LANG_CROATIAN;
    if(LoadNewResourceModule(CROATIAN)) ReinitializeForms();
}

void __fastcall Tmain_form::Button2Click(TObject *Sender) {
    const int ENGLISH = (SUBLANG_ENGLISH_US << 10) | LANG_ENGLISH;
    if(LoadNewResourceModule(ENGLISH)) ReinitializeForms();
}

