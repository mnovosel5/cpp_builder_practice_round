﻿#include <vcl.h>
#pragma hdrstop

#include "Unit4.h"
#include "Unit5.h"
#include "reinit.hpp"

#pragma package(smart_init)
#pragma resource "*.dfm"

Tmain_form *main_form;
UnicodeString language;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {

}

void reset_data() {
    main_form->main_memo->Clear();
    main_form->combo_newspaper->ItemIndex = 0;
    main_form->radio_a->Checked = true;
    main_form->name_edit->Text = "Marko";
    main_form->surname_edit->Text = "Markovic";
    main_form->email_edit->Text = "mmarkovic@gmail.com";
}

void __fastcall Tmain_form::FormCreate(TObject *Sender) {
    /*

		When the form loads, clear the memo text and set the default value
		of the components needed for the input

    */
	reset_data();
}

// Validate function for the user data
bool validate_input() {
    bool valid = false;
    if(main_form->name_edit->Text.Length != 0
		&& main_form->surname_edit->Text.Length != 0
		&& main_form->email_edit->Text.Length != 0
		&& main_form->agree_checkbox->Checked == true
	) {
        valid = true;
	}
    return valid;
}

void __fastcall Tmain_form::subscribe_buttonClick(TObject *Sender) {
    UnicodeString period = NULL; // Unassigned for now

    // Find out which radio button is selected and get the caption string
    TRadioButton *radio_buttons[3];
    radio_buttons[0] = radio_a;
    radio_buttons[1] = radio_b;
    radio_buttons[2] = radio_c;

    for(int i = 0; i < 3; i++) {
        if(radio_buttons[i]->Checked) {
            period = radio_buttons[i]->Caption;
            break;
		}
	}

    // Validate the input
    if(validate_input()) {
        if(language == "hrv") {
            main_memo->Text =
                "Ime: " + name_edit->Text + "\r\n" +
                "Prezime: " + surname_edit->Text + "\r\n" +
                "e-mail: " + email_edit->Text + "\r\n" +
                "Datum rođenja: " + date_picker->Date.FormatString("dd.MM.yyyy") + "\r\n" +
                "Novine: " + combo_newspaper->Text + "\r\n" +
                "Vrsta pretplate: " + period;
		} else {
            main_memo->Text =
                "Name: " + name_edit->Text + "\r\n" +
                "Surname: " + surname_edit->Text + "\r\n" +
                "e-mail: " + email_edit->Text + "\r\n" +
                "Date of birth: " + date_picker->Date.FormatString("dd.MM.yyyy") + "\r\n" +
                "Newspaper: " + combo_newspaper->Text + "\r\n" +
                "Period: " + period;
		}
	}
}

void __fastcall Tmain_form::save_buttonClick(TObject *Sender) {
    UnicodeString file_name;
    if(save_dialog->Execute() == true) {
        TStringList *list = new TStringList;
        list->Add(main_memo->Text);
        UnicodeString extension = ExtractFileExt(save_dialog->FileName);
        if(extension != ".txt" && extension != ".rtf") {
           	file_name = save_dialog->FileName + ".txt";
		} else {
            file_name = save_dialog->FileName;
		}
        list->SaveToFile(file_name, TEncoding::UTF8);
        delete list;
    }
}

void __fastcall Tmain_form::New1Click(TObject *Sender) {
    reset_data();
}

void __fastcall Tmain_form::About1Click(TObject *Sender) {
    Tabout_form *form = new Tabout_form(this);
    form->ShowModal();
    delete form;
}

void __fastcall Tmain_form::hrv_buttonClick(TObject *Sender) {
    language = "hrv";
    const int CROATIAN = (SUBLANG_CROATIAN_CROATIA << 10) | LANG_CROATIAN;
    if(LoadNewResourceModule(CROATIAN)) {
        ReinitializeForms();
        reset_data();
	}
}

void __fastcall Tmain_form::eng_buttonClick(TObject *Sender) {
    language = "eng";
	const int ENGLISH = (SUBLANG_ENGLISH_US << 10) | LANG_ENGLISH;
    if(LoadNewResourceModule(ENGLISH)) {
        ReinitializeForms();
        reset_data();
	}
}
