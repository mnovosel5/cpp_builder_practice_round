object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'Pretplata za novine'
  ClientHeight = 391
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = main_menu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 305
    Height = 137
    Caption = 'Korisni'#269'ki podatci'
    TabOrder = 0
    object label_name: TLabel
      Left = 48
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Ime:'
    end
    object Label2: TLabel
      Left = 33
      Top = 51
      Width = 46
      Height = 13
      Caption = 'Prezime:'
    end
    object Label3: TLabel
      Left = 47
      Top = 78
      Width = 32
      Height = 13
      Caption = 'E-mail:'
    end
    object Label4: TLabel
      Left = 28
      Top = 102
      Width = 51
      Height = 13
      Caption = 'Datum ro'#273'enja:'
    end
    object name_edit: TEdit
      Left = 85
      Top = 21
      Width = 204
      Height = 21
      TabOrder = 0
    end
    object surname_edit: TEdit
      Left = 85
      Top = 48
      Width = 204
      Height = 21
      TabOrder = 1
    end
    object email_edit: TEdit
      Left = 85
      Top = 75
      Width = 204
      Height = 21
      TabOrder = 2
    end
    object date_picker: TDateTimePicker
      Left = 85
      Top = 102
      Width = 204
      Height = 21
      Date = 43543
      Time = 0.424345358798746000
      TabOrder = 3
    end
  end
  object main_memo: TMemo
    Left = 319
    Top = 16
    Width = 233
    Height = 313
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    WantReturns = False
    WordWrap = False
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 151
    Width = 233
    Height = 74
    Caption = 'Odabir novina'
    TabOrder = 2
    object combo_newspaper: TComboBox
      Left = 16
      Top = 32
      Width = 201
      Height = 21
      TabOrder = 0
      Items.Strings = (
        'Ve'#269'ernji'
        '24 Sata'
        'Jutarnji'
        'Slobodna Dalmacija')
    end
  end
  object radio_subscription: TRadioGroup
    Left = 8
    Top = 231
    Width = 233
    Height = 98
    Caption = 'Vrsta pretplate'
    TabOrder = 3
  end
  object radio_a: TRadioButton
    Left = 80
    Top = 252
    Width = 113
    Height = 17
    Caption = '6 mjeseci'
    TabOrder = 4
  end
  object radio_b: TRadioButton
    Left = 80
    Top = 275
    Width = 113
    Height = 17
    Caption = '12 mjeseci'
    TabOrder = 5
  end
  object radio_c: TRadioButton
    Left = 80
    Top = 298
    Width = 113
    Height = 17
    Caption = '24 mjeseca'
    TabOrder = 6
  end
  object subscribe_button: TButton
    Left = 41
    Top = 358
    Width = 75
    Height = 25
    Caption = 'Pretplati me'
    TabOrder = 7
    OnClick = subscribe_buttonClick
  end
  object Panel1: TPanel
    Left = 247
    Top = 151
    Width = 66
    Height = 178
    Color = clMenu
    ParentBackground = False
    TabOrder = 8
  end
  object save_button: TButton
    Left = 122
    Top = 358
    Width = 75
    Height = 25
    Caption = 'Spremi'
    TabOrder = 9
    OnClick = save_buttonClick
  end
  object agree_checkbox: TCheckBox
    Left = 8
    Top = 335
    Width = 153
    Height = 17
    Caption = 'Sla'#382'em se s pravilima'
    TabOrder = 10
  end
  object hrv_button: TButton
    Left = 475
    Top = 335
    Width = 75
    Height = 25
    Caption = 'HRV'
    TabOrder = 11
    OnClick = hrv_buttonClick
  end
  object eng_button: TButton
    Left = 475
    Top = 358
    Width = 75
    Height = 25
    Caption = 'ENG'
    TabOrder = 12
    OnClick = eng_buttonClick
  end
  object save_dialog: TSaveDialog
    Filter = 
      'All Files(*.*)||Text Documents (*.txt)|*.txt|Rich Text Format (*' +
      '.rtf)|*.rtf'
    Left = 424
    Top = 152
  end
  object main_menu: TMainMenu
    Left = 424
    Top = 208
    object N1: TMenuItem
      Caption = 'Pretplata'
      object New1: TMenuItem
        Caption = 'Novo'
        OnClick = New1Click
      end
      object About1: TMenuItem
        Caption = 'O programu'
        OnClick = About1Click
      end
    end
  end
end
