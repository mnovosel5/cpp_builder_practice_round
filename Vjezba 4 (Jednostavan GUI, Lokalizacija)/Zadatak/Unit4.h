#ifndef Unit4H
#define Unit4H

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtDlgs.hpp>
#include <Vcl.Menus.hpp>

class Tmain_form : public TForm {
    __published:
		// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *label_name;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TGroupBox *GroupBox2;
        TComboBox *combo_newspaper;
        TRadioGroup *radio_subscription;
        TRadioButton *radio_a;
        TRadioButton *radio_b;
        TRadioButton *radio_c;
        TButton *subscribe_button;
        TPanel *Panel1;
        TButton *save_button;
        TEdit *name_edit;
        TEdit *surname_edit;
        TEdit *email_edit;
        TDateTimePicker *date_picker;
        TMemo *main_memo;
        TCheckBox *agree_checkbox;
	TSaveDialog *save_dialog;
	TMainMenu *main_menu;
	TMenuItem *N1;
	TMenuItem *New1;
	TMenuItem *About1;
	TButton *hrv_button;
	TButton *eng_button;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall subscribe_buttonClick(TObject *Sender);
	void __fastcall save_buttonClick(TObject *Sender);
	void __fastcall New1Click(TObject *Sender);
	void __fastcall About1Click(TObject *Sender);
	void __fastcall hrv_buttonClick(TObject *Sender);
	void __fastcall eng_buttonClick(TObject *Sender);
    private:
		// User declarations
    public:
		// User declarations
        __fastcall Tmain_form(TComponent* Owner);
};

extern PACKAGE Tmain_form *main_form;

#endif
