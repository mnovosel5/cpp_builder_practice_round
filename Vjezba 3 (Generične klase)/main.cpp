#include<iostream>
#include<string>
#include<random>
using namespace std;

class Exception {
    public:
        string message;
        Exception(string _message) {
            message = _message;
		}
};

template<class type>
class Stack {
    public:
        type *contents;
        int top, size;
        Stack(int N) {
            try {
                if(N > 100) {
                    throw(Exception("Maximum size of stack is 100 elements!"));
				} else {
                    contents = new type[N];
                    top = -1;
                    size = N;
				}
			} catch(const Exception & e) {
                cout << e.message << endl;
			}
		}
        bool push(type value) {
            try {
                // Full stack
                if(top > (size - 1)) {
                    throw(Exception("Stack is full!"));
                } else {
                    contents[++top] = value;
                    return(true);
                }
			} catch(const Exception & e) {
                cout << e.message << endl;
                return(false);
			}
		}
        bool pop(type & value) {
            try {
                // Empty stack
                if(top == -1) {
                    throw(Exception("Stack is empty!"));
                } else {
                    value = contents[top--];
                    return(true);
                }
			} catch(const Exception & e) {
                cout << e.message << endl;
                return(false);
			}
        }
        ~Stack() {
            delete[] contents;
		}
};

int rand_int(int low, int high) {
    random_device dev;
    mt19937 rng(dev());
    uniform_int_distribution<mt19937::result_type> dist6(low, high); // distribution in range [1, 6]
    return(dist6(rng));
}

template<class type>
void sort(type *array, int n) {
    for(int i = 0; i < n; i++) {
        for(int j = i + 1; j < n; j++) {
            if(array[i] > array[j]) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
			}
        }
	}
}

int main() {
    int *array = new int[20];
    for(int i = 0; i < 20; i++) {
        array[i] = rand_int(1, 20);
        cout << "array[" << i << "] = " << array[i] << endl;
	}
    sort(array, 20);
    // After sorting
	for(int i = 0; i < 20; i++) {
        cout << "array[" << i << "] = " << array[i] << endl;
	}

    // Error, stack will be uninitialized
    Stack<int> stack_int_0 = Stack<int>(101);
    // Successful stack creation
    Stack<int> stack_int_1 = Stack<int>(10);

    // Popping a value when the stack is empty
	int value;
	bool success = stack_int_1.pop(value);
	cout << value << " -> " << (success ? "true" : "false") << endl;

    // Push two values
	stack_int_1.push(6);
    stack_int_1.push(24);

	success = stack_int_1.pop(value);
    cout << value << " -> " << (success ? "true" : "false") << endl;

	success = stack_int_1.pop(value);
    cout << value << " -> " << (success ? "true" : "false") << endl;

    // Try to pop one off the stack
	success = stack_int_1.pop(value);
    cout << value << " -> " << (success ? "true" : "false") << endl;

	system("pause");
    return 0;
}
