// ---------------------------------------------------------------------------
// ComplexNumberImpl.h : Declaration of the TComplexNumberImpl
// ---------------------------------------------------------------------------
#ifndef ComplexNumberImplH
#define ComplexNumberImplH

#include <System.Win.ComServ.hpp>
#include <axbase.h>
#include "ComplexTesting_TLB.h"

// ---------------------------------------------------------------------------
// TComplexNumberImpl     Implements IComplexNumber, default interface of ComplexNumber
// ThreadingModel : tmApartment
// Dual Interface : FALSE
// Event Support  : FALSE
// Description    : 
// ---------------------------------------------------------------------------
class DAX_COM_CLASS TComplexNumberImpl : public TCppComObject<IComplexNumber>
{
  typedef _COM_CLASS inherited;

public:
  __fastcall TComplexNumberImpl();
  __fastcall TComplexNumberImpl(const System::_di_IInterface Controller);
  __fastcall TComplexNumberImpl(Comobj::TComObjectFactory* Factory, const System::_di_IInterface Controller);
  

  // IComplexNumber
protected:
  double STDMETHODCALLTYPE Modul();
  double STDMETHODCALLTYPE get_Re();
  void STDMETHODCALLTYPE set_Re(double _re);
  double STDMETHODCALLTYPE get_Im();
  void STDMETHODCALLTYPE set_Im(double _im);
  double Re;
  double Im;
};




#endif //ComplexNumberImplH
