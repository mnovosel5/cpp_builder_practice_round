// ---------------------------------------------------------------------------
// COMPLEXNUMBERIMPL : Implementation of TComplexNumberImpl (CoClass: ComplexNumber, Interface: IComplexNumber)
// ---------------------------------------------------------------------------
#include <vcl.h>
#include<cmath>
#pragma hdrstop

#include "ComplexNumberImpl.h"


// ---------------------------------------------------------------------------
// TComplexNumberImpl
// ---------------------------------------------------------------------------
__fastcall TComplexNumberImpl::TComplexNumberImpl()
{
}


// ---------------------------------------------------------------------------
// TComplexNumberImpl
// ---------------------------------------------------------------------------
__fastcall TComplexNumberImpl::TComplexNumberImpl(const System::_di_IInterface Controller)
                              : inherited(Controller)
{
}


// ---------------------------------------------------------------------------
// TComplexNumberImpl
// ---------------------------------------------------------------------------
__fastcall TComplexNumberImpl::TComplexNumberImpl(Comobj::TComObjectFactory* Factory,
                                      const System::_di_IInterface Controller)
                              : inherited(Factory, Controller)
{
}

// ---------------------------------------------------------------------------
// TComplexNumberImpl - Class Factory
// ---------------------------------------------------------------------------
static void createFactory()
{
  new TCppComObjectFactory<TComplexNumberImpl>(Comserv::GetComServer(),
                           __classid(TComplexNumberImpl),
                           CLSID_ComplexNumber,
                           "TComplexNumberImpl",
                           "",
                           Comobj::ciMultiInstance,
                           Comobj::tmApartment);
}
#pragma startup createFactory 32

double STDMETHODCALLTYPE TComplexNumberImpl::Modul()
{
	return(sqrt(Re * Re + Im * Im));
}

double STDMETHODCALLTYPE TComplexNumberImpl::get_Re()
{
	return(Re);
}


void STDMETHODCALLTYPE TComplexNumberImpl::set_Re(double _re)
{
	Re = _re;
}



double STDMETHODCALLTYPE TComplexNumberImpl::get_Im()
{
	return(Im);
}


void STDMETHODCALLTYPE TComplexNumberImpl::set_Im(double _im)
{
	Im = _im;
}



