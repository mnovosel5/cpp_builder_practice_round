// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 92848 $
// File generated on 4/28/2019 1:06:45 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Zadatak\Project1 (1)
// LIBID: {1DE16CC1-5AF4-4EBF-AC5D-55534EEED0A3}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "Project1_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Project1_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary
// *********************************************************************//
const GUID LIBID_Project1 = {0x1DE16CC1, 0x5AF4, 0x4EBF,{ 0xAC, 0x5D, 0x55,0x53, 0x4E, 0xEE,0xD0, 0xA3} };

};     // namespace Project1_tlb

