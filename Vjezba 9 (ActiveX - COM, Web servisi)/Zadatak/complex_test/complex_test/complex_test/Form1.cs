﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace complex_test
{
	public partial class Form1: Form
	{
		private ComplexTesting.ComplexNumber complex_number;
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			complex_number = new ComplexTesting.ComplexNumber();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			complex_number.Im = float.Parse(textBox2.Text);
			complex_number.Re = float.Parse(textBox1.Text);
			MessageBox.Show("z = " + complex_number.Re + (complex_number.Im > 0 ? "+" : "") + complex_number.Im + "i, modul = " + complex_number.Modul().ToString());
		}
	}
}
