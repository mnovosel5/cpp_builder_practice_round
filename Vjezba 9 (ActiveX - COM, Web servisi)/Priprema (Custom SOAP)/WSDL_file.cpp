// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (Custom SOAP)\WSDL_file.xml
//  >Import : C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (Custom SOAP)\WSDL_file.xml>0
//  >Import : C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (Custom SOAP)\WSDL_file.xml>1
// Version  : 1.0
// (4/28/2019 12:42:18 PM - - $Rev: 93209 $)
// ************************************************************************ //

#include <System.hpp>
#pragma hdrstop

#include "WSDL_file.h"



namespace NS_WSDL_file {

_di_IWS_testing GetIWS_testing(bool useWSDL, System::String addr, Soaphttpclient::THTTPRIO* HTTPRIO)
{
  static const char* defWSDL= "C:\\Users\\mnovosel2\\Desktop\\repositories\\cpp_builder_practice_round\\vjezba_9 (ActiveX - COM, Web servisi)\\Priprema (Custom SOAP)\\WSDL_file.xml";
  static const char* defURL = "http://localhost:8080/soap/IWS_testing";
  static const char* defSvc = "IWS_testingservice";
  static const char* defPrt = "IWS_testingPort";
  if (addr=="")
    addr = useWSDL ? defWSDL : defURL;
  Soaphttpclient::THTTPRIO* rio = HTTPRIO ? HTTPRIO : new Soaphttpclient::THTTPRIO(0);
  if (useWSDL) {
    rio->WSDLLocation = addr;
    rio->Service = defSvc;
    rio->Port = defPrt;
  } else {
    rio->URL = addr;
  }
  _di_IWS_testing service;
  rio->QueryInterface(service);
  if (!service && !HTTPRIO)
    delete rio;
  return service;
}


// ************************************************************************ //
// This routine registers the interfaces and types exposed by the WebService.
// ************************************************************************ //
static void RegTypes()
{
  /* IWS_testing */
  InvRegistry()->RegisterInterface(__delphirtti(IWS_testing), L"urn:WS_testing-IWS_testing", L"");
  InvRegistry()->RegisterDefaultSOAPAction(__delphirtti(IWS_testing), L"urn:WS_testing-IWS_testing#%operationName%");
  /* IWS_testing->echoEnum */
  InvRegistry()->RegisterParamInfo(__delphirtti(IWS_testing), "echoEnum", "return_", L"return", L"");
  /* IWS_testing->echoDoubleArray */
  InvRegistry()->RegisterParamInfo(__delphirtti(IWS_testing), "echoDoubleArray", "return_", L"return", L"");
  /* IWS_testing->echoStruct */
  InvRegistry()->RegisterParamInfo(__delphirtti(IWS_testing), "echoStruct", "return_", L"return", L"");
  /* IWS_testing->echoDouble */
  InvRegistry()->RegisterParamInfo(__delphirtti(IWS_testing), "echoDouble", "return_", L"return", L"");
  /* IWS_testing->sum */
  InvRegistry()->RegisterParamInfo(__delphirtti(IWS_testing), "sum", "return_", L"return", L"");
  /* TDoubleDynArray */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(TDoubleDynArray), L"urn:WS_testing", L"TDoubleDynArray");
  /* TSampleStruct */
  RemClassRegistry()->RegisterXSClass(__classid(TSampleStruct), L"urn:@:WS_testing", L"TSampleStruct");
  /* SampleEnum */
  RemClassRegistry()->RegisterXSInfo(GetClsMemberTypeInfo(__typeinfo(SampleEnum_TypeInfoHolder)), L"urn:WS_testing", L"SampleEnum");
}
#pragma startup RegTypes 32

};     // NS_WSDL_file

