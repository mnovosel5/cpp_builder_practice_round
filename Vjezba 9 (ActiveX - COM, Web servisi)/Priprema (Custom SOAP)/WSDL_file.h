// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (Custom SOAP)\WSDL_file.xml
//  >Import : C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (Custom SOAP)\WSDL_file.xml>0
//  >Import : C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (Custom SOAP)\WSDL_file.xml>1
// Version  : 1.0
// (4/28/2019 12:42:18 PM - - $Rev: 93209 $)
// ************************************************************************ //

#ifndef   WSDL_fileH
#define   WSDL_fileH

#include <System.hpp>
#include <Soap.InvokeRegistry.hpp>
#include <Soap.XSBuiltIns.hpp>
#include <Soap.SOAPHTTPClient.hpp>

#if !defined(SOAP_REMOTABLE_CLASS)
#define SOAP_REMOTABLE_CLASS __declspec(delphiclass)
#endif

namespace NS_WSDL_file {

// ************************************************************************ //
// The following types, referred to in the WSDL document are not being represented
// in this file. They are either aliases[@] of other types represented or were referred
// to but never[!] declared in the document. The types from the latter category
// typically map to predefined/known XML or Embarcadero types; however, they could also 
// indicate incorrect WSDL documents that failed to declare or import a schema type.
// ************************************************************************ //
// !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:int             - "http://www.w3.org/2001/XMLSchema"[]
// !:double          - "http://www.w3.org/2001/XMLSchema"[Gbl]

class SOAP_REMOTABLE_CLASS TSampleStruct;

enum class SampleEnum   /* "urn:WS_testing"[GblSmpl] */
{
  etNone, 
  etAFew, 
  etSome, 
  etALot
};

class SampleEnum_TypeInfoHolder : public TObject {
  SampleEnum __instanceType;
public:
__published:
  __property SampleEnum __propType = { read=__instanceType };
};

typedef DynamicArray<double>      TDoubleDynArray; /* "urn:WS_testing"[GblCplx] */


// ************************************************************************ //
// XML       : TSampleStruct, global, <complexType>
// Namespace : urn:@:WS_testing
// ************************************************************************ //
class TSampleStruct : public TRemotable {
private:
  UnicodeString   FLastName;
  UnicodeString   FFirstName;
  double          FSalary;
__published:
  __property UnicodeString   LastName = { read=FLastName, write=FLastName };
  __property UnicodeString  FirstName = { read=FFirstName, write=FFirstName };
  __property double         Salary = { read=FSalary, write=FSalary };
};



// ************************************************************************ //
// Namespace : urn:WS_testing-IWS_testing
// soapAction: urn:WS_testing-IWS_testing#%operationName%
// transport : http://schemas.xmlsoap.org/soap/http
// style     : rpc
// use       : encoded
// binding   : IWS_testingbinding
// service   : IWS_testingservice
// port      : IWS_testingPort
// URL       : http://localhost:8080/soap/IWS_testing
// ************************************************************************ //
__interface INTERFACE_UUID("{94ADA469-07B0-D82E-E151-601F4DD6C3FB}") IWS_testing : public IInvokable
{
public:
  virtual SampleEnum      echoEnum(const SampleEnum eValue) = 0; 
  virtual TDoubleDynArray echoDoubleArray(const TDoubleDynArray daValue) = 0; 
  virtual TSampleStruct*  echoStruct(const TSampleStruct* pEmployee) = 0; 
  virtual double          echoDouble(const double dValue) = 0; 
  virtual int             sum(const int a, const int b) = 0; 
};
typedef DelphiInterface<IWS_testing> _di_IWS_testing;

_di_IWS_testing GetIWS_testing(bool useWSDL=false, System::String addr= System::String(), Soaphttpclient::THTTPRIO* HTTPRIO=0);


};     // NS_WSDL_file

#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using  namespace NS_WSDL_file;
#endif



#endif // WSDL_fileH
