// ************************************************************************ //
// Implementation class for interface IWS_testing
// ************************************************************************ //
#include <stdio.h>
#include <vcl.h>
#pragma hdrstop

#if !defined(__WS_testing_h__)
#include "WS_testing.h"
#endif

// ************************************************************************ //
//  TWS_testingImpl implements interface IWS_testing
// ************************************************************************ //
class TWS_testingImpl : public TInvokableClass, public IWS_testing
{
public:
    /* Sample methods of IWS_testing */
  SampleEnum     echoEnum(SampleEnum eValue);
  TDoubleArray   echoDoubleArray(const TDoubleArray daValue);
  TSampleStruct* echoStruct(const TSampleStruct* pStruct);
  double         echoDouble(double dValue);
  int sum(int a, int b) { return(a + b); };

  /* IUnknown */
  HRESULT STDMETHODCALLTYPE QueryInterface(const GUID& IID, void **Obj)
                        { return GetInterface(IID, Obj) ? S_OK : E_NOINTERFACE; }
  ULONG STDMETHODCALLTYPE AddRef() { return TInvokableClass::_AddRef();  }
  ULONG STDMETHODCALLTYPE Release() { return TInvokableClass::_Release();  }
};


SampleEnum TWS_testingImpl::echoEnum(SampleEnum eValue)
{
  /* TODO : Implement method echoEnum */
  return eValue;
}

TDoubleArray TWS_testingImpl::echoDoubleArray(TDoubleArray daValue)
{
  /* TODO : Implement method echoDoubleArray */
  return daValue;
}

TSampleStruct* TWS_testingImpl::echoStruct(const TSampleStruct* pEmployee)
{
  /* TODO : Implement method echoMyEmployee */
  return new TSampleStruct();
}

double TWS_testingImpl::echoDouble(const double dValue)
{
  /* TODO : Implement method echoDouble */
  return dValue;
}


static void __fastcall WS_testingFactory(System::TObject* &obj)
{
  static _di_IWS_testing iInstance;
  static TWS_testingImpl *instance = 0;
  if (!instance)
  {
    instance = new TWS_testingImpl();
    instance->GetInterface(iInstance);
  }
  obj = instance;
}

// ************************************************************************ //
//  The following routine registers the interface and implementation class
//  as well as the type used by the methods of the interface
// ************************************************************************ //
static void RegTypes()
{
  InvRegistry()->RegisterInterface(__delphirtti(IWS_testing));
  InvRegistry()->RegisterInvokableClass(__classid(TWS_testingImpl), WS_testingFactory);
}
#pragma startup RegTypes 32

