// ---------------------------------------------------------------------------
// com_objectImpl.h : Declaration of the Tcom_objectImpl
// ---------------------------------------------------------------------------
#ifndef com_objectImplH
#define com_objectImplH

#include <System.Win.ComServ.hpp>
#include <axbase.h>
#include "Component_TLB.h"

// ---------------------------------------------------------------------------
// Tcom_objectImpl     Implements Icom_object, default interface of com_object
// ThreadingModel : tmApartment
// Dual Interface : FALSE
// Event Support  : FALSE
// Description    : 
// ---------------------------------------------------------------------------
class DAX_COM_CLASS Tcom_objectImpl : public TCppComObject<Icom_object>
{
  typedef _COM_CLASS inherited;

public:
  __fastcall Tcom_objectImpl();
  __fastcall Tcom_objectImpl(const System::_di_IInterface Controller);
  __fastcall Tcom_objectImpl(Comobj::TComObjectFactory* Factory, const System::_di_IInterface Controller);
  

  // Icom_object
protected:
  STDMETHOD(message_method());
};




#endif //com_objectImplH
