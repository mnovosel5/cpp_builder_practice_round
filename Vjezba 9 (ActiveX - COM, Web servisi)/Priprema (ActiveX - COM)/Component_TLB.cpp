// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 92848 $
// File generated on 4/28/2019 11:53:11 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\vjezba_9 (ActiveX - COM, Web servisi)\Priprema (ActiveX - COM)\Project1 (1)
// LIBID: {61E59191-C562-4C24-924A-9465694A33D0}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "Component_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Component_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary
// *********************************************************************//
const GUID LIBID_Component = {0x61E59191, 0xC562, 0x4C24,{ 0x92, 0x4A, 0x94,0x65, 0x69, 0x4A,0x33, 0xD0} };
const GUID IID_Icom_object = {0x473D5C4A, 0x72B7, 0x4CE9,{ 0xAD, 0x59, 0x77,0x62, 0xE0, 0x52,0xFC, 0xD1} };
const GUID CLSID_com_object = {0x64B88AD8, 0xB7D7, 0x4D0C,{ 0xBE, 0xBB, 0xC0,0x74, 0x60, 0xD9,0xAE, 0x41} };

};     // namespace Component_tlb

