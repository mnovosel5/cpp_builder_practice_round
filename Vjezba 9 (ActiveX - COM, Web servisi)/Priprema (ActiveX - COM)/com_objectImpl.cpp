// ---------------------------------------------------------------------------
// COM_OBJECTIMPL : Implementation of Tcom_objectImpl (CoClass: com_object, Interface: Icom_object)
// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "com_objectImpl.h"


// ---------------------------------------------------------------------------
// Tcom_objectImpl
// ---------------------------------------------------------------------------
__fastcall Tcom_objectImpl::Tcom_objectImpl()
{
}


// ---------------------------------------------------------------------------
// Tcom_objectImpl
// ---------------------------------------------------------------------------
__fastcall Tcom_objectImpl::Tcom_objectImpl(const System::_di_IInterface Controller)
                              : inherited(Controller)
{
}


// ---------------------------------------------------------------------------
// Tcom_objectImpl
// ---------------------------------------------------------------------------
__fastcall Tcom_objectImpl::Tcom_objectImpl(Comobj::TComObjectFactory* Factory,
                                      const System::_di_IInterface Controller)
                              : inherited(Factory, Controller)
{
}

// ---------------------------------------------------------------------------
// Tcom_objectImpl - Class Factory
// ---------------------------------------------------------------------------
static void createFactory()
{
  new TCppComObjectFactory<Tcom_objectImpl>(Comserv::GetComServer(),
                           __classid(Tcom_objectImpl),
                           CLSID_com_object,
                           "Tcom_objectImpl",
                           "",
                           Comobj::ciMultiInstance,
                           Comobj::tmApartment);
}
#pragma startup createFactory 32


STDMETHODIMP Tcom_objectImpl::message_method() {
  try {
    ShowMessage("Hello from C++ builder!");
  } catch(Exception &e) {
    return Error(e.Message.c_str(), IID_Icom_object);
  }
  return S_OK;
}


