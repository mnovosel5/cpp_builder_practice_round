#include <vcl.h>
#pragma hdrstop

#include "Unit6.h"
#include "Unit5.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner) : TForm(Owner) {

}

void __fastcall Tmain_form::FormCreate(TObject *Sender) {
    rich_edit->Clear();
}

void __fastcall Tmain_form::About1Click(TObject *Sender) {
    Tabout_form *form = new Tabout_form(this);
    form->ShowModal();
    delete form;
}

void __fastcall Tmain_form::Openfile1Click(TObject *Sender) {
    UnicodeString file_name;
    if(open_dialog->Execute() == true) {
        TStringList *list = new TStringList;
        list->LoadFromFile(open_dialog->FileName, TEncoding::UTF8);
        rich_edit->Text = list[0].Text;
        delete list;
    }
}

void __fastcall Tmain_form::Savefileas1Click(TObject *Sender) {
    UnicodeString file_name;
    if(save_dialog->Execute() == true) {
        TStringList *list = new TStringList;
        list->Add(rich_edit->Text);
        UnicodeString extension = ExtractFileExt(save_dialog->FileName);
        if(extension != ".txt" && extension != ".rtf") {
           	file_name = save_dialog->FileName + ".txt";
		} else {
            file_name = save_dialog->FileName;
		}
        list->SaveToFile(file_name, TEncoding::UTF8);
		delete list;
    }
}

void __fastcall Tmain_form::ToolButton11Click(TObject *Sender) {
    UnicodeString file_name;
    if(save_dialog->Execute() == true) {
        TStringList *list = new TStringList;
        list->Add(rich_edit->Text);
        UnicodeString extension = ExtractFileExt(save_dialog->FileName);
        if(extension != ".txt" && extension != ".rtf") {
           	file_name = save_dialog->FileName + ".txt";
		} else {
            file_name = save_dialog->FileName;
		}
        list->SaveToFile(file_name, TEncoding::UTF8);
        delete list;
    }
}

void __fastcall Tmain_form::ToolButton8Click(TObject *Sender) {
	UnicodeString file_name;
    if(open_dialog->Execute() == true) {
        TStringList *list = new TStringList;
        list->LoadFromFile(open_dialog->FileName, TEncoding::UTF8);
        rich_edit->Text = list[0].Text;
        delete list;
    }
}

void __fastcall Tmain_form::ToolButton15Click(TObject *Sender) {
    Tabout_form *form = new Tabout_form(this);
    form->ShowModal();
    delete form;
}
