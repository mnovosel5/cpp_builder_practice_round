//---------------------------------------------------------------------------

#ifndef Unit6H
#define Unit6H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ActnCtrls.hpp>
#include <Vcl.ActnMan.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.StdActns.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ExtActns.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tool_bar_top;
	TToolButton *ToolButton1;
	TRichEdit *rich_edit;
	TActionList *action_list;
	TImageList *image_list;
	TEditSelectAll *EditSelectAll1;
	TToolButton *ToolButton3;
	TEditDelete *EditDelete1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton5;
	TToolButton *ToolButton6;
	TEditCopy *EditCopy1;
	TEditCut *EditCut1;
	TEditPaste *EditPaste1;
	TStatusBar *StatusBar1;
	TToolButton *ToolButton7;
	TEditUndo *EditUndo1;
	TOpenDialog *open_dialog;
	TToolButton *ToolButton4;
	TToolButton *ToolButton9;
	TToolButton *ToolButton10;
	TMainMenu *main_menu;
	TMenuItem *File1;
	TMenuItem *Openfile1;
	TMenuItem *Savefileas1;
	TMenuItem *About1;
	TSaveDialog *save_dialog;
	TFileOpen *FileOpen1;
	TFileSaveAs *FileSaveAs1;
	TFileOpen *FileOpen2;
	TToolButton *ToolButton8;
	TToolButton *ToolButton11;
	TAction *Action1;
	TFileOpen *FileOpen3;
	TFileSaveAs *FileSaveAs2;
	TToolButton *ToolButton12;
	TToolButton *ToolButton13;
	TToolButton *ToolButton14;
	TRichEditAlignLeft *FormatRichEditAlignLeft1;
	TRichEditAlignRight *FormatRichEditAlignRight1;
	TRichEditAlignCenter *FormatRichEditAlignCenter1;
	TToolButton *ToolButton15;
	TToolButton *ToolButton16;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Openfile1Click(TObject *Sender);
	void __fastcall About1Click(TObject *Sender);
	void __fastcall Savefileas1Click(TObject *Sender);
	void __fastcall ToolButton11Click(TObject *Sender);
	void __fastcall ToolButton8Click(TObject *Sender);
	void __fastcall ToolButton15Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
