//---------------------------------------------------------------------------

#ifndef Unit5H
#define Unit5H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class Tabout_form : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *Data;
	TLabel *label_made_by;
	TLabel *Label1;
	TLabel *Label2;
private:	// User declarations
public:		// User declarations
	__fastcall Tabout_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tabout_form *about_form;
//---------------------------------------------------------------------------
#endif
