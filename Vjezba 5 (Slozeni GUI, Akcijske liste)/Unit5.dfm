object about_form: Tabout_form
  Left = 0
  Top = 0
  Caption = 'About'
  ClientHeight = 154
  ClientWidth = 259
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Data: TGroupBox
    Left = 8
    Top = 8
    Width = 241
    Height = 137
    Caption = 'Data'
    TabOrder = 0
    object label_made_by: TLabel
      Left = 24
      Top = 29
      Width = 72
      Height = 19
      Caption = 'Made by:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 40
      Top = 54
      Width = 134
      Height = 25
      Caption = 'Matija Novosel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 40
      Top = 85
      Width = 189
      Height = 25
      Caption = 'JMBAG: 0246073749'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
end
