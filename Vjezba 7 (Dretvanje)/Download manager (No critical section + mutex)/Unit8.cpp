//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include "Unit1.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
    // Mutex creation
    hMutex = CreateMutex(NULL, FALSE, L"mutex_name");
    if(hMutex == NULL) {
        ShowMessage(GetLastError());
	} else {
        if(GetLastError() == ERROR_ALREADY_EXISTS) {
            ShowMessage("Application already started!");
            Application->Terminate();
		}
	}
    progress_bars[0] = progress_bar_0;
    progress_bars[1] = progress_bar_1;
    progress_bars[2] = progress_bar_2;
    download_labels[0] = download_label_0;
    download_labels[1] = download_label_1;
    download_labels[2] = download_label_2;
    labels[0] = label_0;
    labels[1] = label_1;
    labels[2] = label_2;
}

void __fastcall Tmain_form::download_buttonClick(TObject *Sender) {
    TDownloadThread *thread[3];
    for(int i = 0; i < 3; i++) {
        thread[i] = new TDownloadThread(true, progress_bars[i], labels[i], download_labels[i]->Text, i);
        thread[i]->FreeOnTerminate = true;
        thread[i]->Start();
        thread[i] = NULL;
	}
}
