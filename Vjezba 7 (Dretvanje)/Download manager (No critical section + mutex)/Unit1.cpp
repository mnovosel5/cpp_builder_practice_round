#include <System.hpp>
#pragma hdrstop

#include "Unit1.h"
#include "Unit8.h"
#pragma package(smart_init)

__fastcall TDownloadThread::TDownloadThread(bool CreateSuspended, TProgressBar *progress_address, TLabel *label_addres, UnicodeString download_link, int counter): TThread(CreateSuspended) {
    this->progress_bar_address = progress_address;
	this->label_address = label_addres;
    this->download_link = download_link;
    this->counter = counter;
}

void __fastcall TDownloadThread::downloadWork(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount) {
    this->progress_bar_address->Position = AWorkCount;
    Application->ProcessMessages();
}

void __fastcall TDownloadThread::downloadWorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax) {
    this->progress_bar_address->Position = 0;
	this->progress_bar_address->Max = AWorkCountMax;
}

void __fastcall TDownloadThread::synchronize_UI() {
    this->label_address->Caption = "Done!";
}

void __fastcall TDownloadThread::Execute() {
    // HTTP client
    TIdHTTP *download_client = new TIdHTTP();
    download_client->OnWork = &downloadWork;
    download_client->OnWorkBegin = &downloadWorkBegin;

    TMemoryStream *memory_stream = new TMemoryStream();
    download_client->Get(this->download_link, memory_stream);
    memory_stream->SaveToFile("C:\\file_" + IntToStr(this->counter) + ".exe");

    Synchronize(synchronize_UI);

    delete memory_stream;
    delete download_client;
}
