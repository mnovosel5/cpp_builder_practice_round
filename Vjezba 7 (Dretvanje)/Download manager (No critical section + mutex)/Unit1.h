#ifndef Unit1H
#define Unit1H

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <Vcl.ComCtrls.hpp>
#include <vector>

class TDownloadThread: public TThread {
    private:
    protected:
        void __fastcall Execute();
    public:
        __fastcall TDownloadThread(bool CreateSuspended, TProgressBar *progress_address, TLabel *label_addres, UnicodeString download_link, int counter);
    	void __fastcall downloadWork(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount);
        void __fastcall downloadWorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax);
        void __fastcall synchronize_UI();
        TProgressBar *progress_bar_address;
        TLabel *label_address;
        UnicodeString download_link;
        int counter;
};

#endif
