object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 123
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object label_0: TLabel
    Left = 399
    Top = 15
    Width = 21
    Height = 14
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object label_1: TLabel
    Left = 399
    Top = 42
    Width = 21
    Height = 14
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object label_2: TLabel
    Left = 399
    Top = 69
    Width = 21
    Height = 14
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object progress_bar_0: TProgressBar
    Left = 279
    Top = 8
    Width = 114
    Height = 21
    TabOrder = 0
  end
  object download_label_0: TEdit
    Left = 8
    Top = 8
    Width = 257
    Height = 21
    TabOrder = 1
    Text = 'https://www.rarlab.com/rar/wrar571b1.exe'
  end
  object download_button: TButton
    Left = 190
    Top = 89
    Width = 75
    Height = 25
    Caption = 'DOWNLOAD'
    TabOrder = 2
    OnClick = download_buttonClick
  end
  object download_label_1: TEdit
    Left = 8
    Top = 35
    Width = 257
    Height = 21
    TabOrder = 3
    Text = 'https://www.rarlab.com/rar/winrar-x64-571b1.exe'
  end
  object progress_bar_1: TProgressBar
    Left = 279
    Top = 35
    Width = 114
    Height = 21
    TabOrder = 4
  end
  object progress_bar_2: TProgressBar
    Left = 279
    Top = 62
    Width = 114
    Height = 21
    TabOrder = 5
  end
  object download_label_2: TEdit
    Left = 8
    Top = 62
    Width = 257
    Height = 21
    TabOrder = 6
    Text = 'https://www.rarlab.com/rar/rarosx-5.7.1b1.tar.gz'
  end
end
