#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include "Unit1.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
    InitializeCriticalSection(&critical_section);
}

void __fastcall Tmain_form::Button1Click(TObject *Sender) {
    n = 0;
    TThreadIncrement *thread[50];
    for(int i = 0; i < 50; i++) thread[i] = new TThreadIncrement(false);
    for(int i = 0; i < 50; i++) {
        thread[i]->WaitFor();
	}
    ShowMessage(n);
}

void __fastcall Tmain_form::FormClose(TObject *Sender, TCloseAction &Action){
	DeleteCriticalSection(&critical_section);
}
