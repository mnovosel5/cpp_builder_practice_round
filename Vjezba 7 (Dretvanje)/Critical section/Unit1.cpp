#include <System.hpp>
#pragma hdrstop

#include "Unit1.h"
#include "Unit8.h"
#pragma package(smart_init)

__fastcall TThreadIncrement::TThreadIncrement(bool CreateSuspended): TThread(CreateSuspended) {
}

void __fastcall TThreadIncrement::Execute() {
    for(int i = 0; i < 100; i++) {
        EnterCriticalSection(&main_form->critical_section);
		++main_form->n;
        LeaveCriticalSection(&main_form->critical_section);
        Sleep(10);
	}
}
