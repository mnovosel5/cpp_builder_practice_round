#ifndef Unit8H
#define Unit8H

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

class Tmain_form: public TForm {
    __published:
		// IDE-managed Components
		TButton *Button1;
		void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    private:
		// User declarations
    public:
		// User declarations
        __fastcall Tmain_form(TComponent* Owner);
        int n;
        CRITICAL_SECTION critical_section;
};

extern PACKAGE Tmain_form *main_form;

#endif
