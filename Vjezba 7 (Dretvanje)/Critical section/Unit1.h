#ifndef Unit1H
#define Unit1H

#include <System.Classes.hpp>

class TThreadIncrement: public TThread {
    private:
    protected:
        void __fastcall Execute();
    public:
        __fastcall TThreadIncrement(bool CreateSuspended);
};

#endif
