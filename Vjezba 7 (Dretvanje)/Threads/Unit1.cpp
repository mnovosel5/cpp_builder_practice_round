#include <System.hpp>
#pragma hdrstop

#include "Unit1.h"
#include "Unit7.h"
#pragma package(smart_init)

__fastcall TSortThread::TSortThread(bool CreateSuspended): TThread(CreateSuspended) {

}

void __fastcall TSortThread::update_caption() {
    main_form->button_sort->Caption = "DONE";
}

void __fastcall TSortThread::Execute() {
    FreeOnTerminate = true;
    for(int i = 0; i < 50000; i++) {
        for(int j = i + 1; j < 50000; j++) {
            if(main_form->data[i] > main_form->data[j]) {
                int temp = main_form->data[i];
                main_form->data[i] = main_form->data[j];
                main_form->data[j] = temp;
			}
		}
	}
    Synchronize(update_caption);
}
