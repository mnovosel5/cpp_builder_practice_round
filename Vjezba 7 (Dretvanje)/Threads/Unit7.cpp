//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit7.h"
#include "Unit1.h"
#include <random>

#pragma package(smart_init)
#pragma resource "*.dfm"

Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
}

int rand_int(int low, int high) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(low, high);
    return(dist6(rng));
}

void __fastcall Tmain_form::button_sortClick(TObject *Sender) {
    data = new int[50000];
    for(int i = 0; i < 50000; i++) data[i] = rand_int(1, 100);
    button_sort->Caption = "SORTING";
    TSortThread *thread = new TSortThread(false);
}
