#ifndef Unit7H
#define Unit7H

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

class Tmain_form: public TForm {
    __published:
		TButton *button_sort;
        void __fastcall button_sortClick(TObject *Sender);
    private:
    public:
        __fastcall Tmain_form(TComponent* Owner);
        int *data;
};

extern PACKAGE Tmain_form *main_form;
#endif
