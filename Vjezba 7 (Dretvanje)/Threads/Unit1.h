#ifndef Unit1H
#define Unit1H

#include <System.Classes.hpp>

class TSortThread: public TThread {
    private:
    protected:
        void __fastcall Execute();
    public:
        __fastcall TSortThread(bool CreateSuspended);
        void __fastcall update_caption();
};

#endif
