#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {

}

void __fastcall Tmain_form::Button1Click(TObject *Sender) {
    STARTUPINFO startInfo;
	PROCESS_INFORMATION processInfo;
	wchar_t CommandLine[255] = L"Project1.exe";
	unsigned long exitcode;
	GetStartupInfo(&startInfo);
	if((CreateProcess(NULL, CommandLine, NULL, NULL, FALSE, 0, NULL, NULL, &startInfo, &processInfo)) == NULL) {
		ShowMessage("Ne mogu kreirati proces!");
		return;
	}
	WaitForSingleObject(processInfo.hProcess, INFINITE);
	GetExitCodeProcess(processInfo.hProcess, &exitcode);
	ShowMessage(exitcode);
}
