#ifndef Unit8H
#define Unit8H

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <Vcl.ComCtrls.hpp>
#include <vector>

class Tmain_form: public TForm {
    __published:
		// IDE-managed Components
        TProgressBar *progress_bar_0;
        TEdit *download_label_0;
        TButton *download_button;
        TEdit *download_label_1;
        TProgressBar *progress_bar_1;
        TProgressBar *progress_bar_2;
        TEdit *download_label_2;
	TLabel *label_0;
	TLabel *label_1;
	TLabel *label_2;
        void __fastcall download_buttonClick(TObject *Sender);
    private:
		// User declarations
    public:
		// User declarations
        __fastcall Tmain_form(TComponent* Owner);
        TProgressBar *progress_bars[3];
    	TEdit *download_labels[3];
        TLabel *labels[3];
};

extern PACKAGE Tmain_form *main_form;

#endif
