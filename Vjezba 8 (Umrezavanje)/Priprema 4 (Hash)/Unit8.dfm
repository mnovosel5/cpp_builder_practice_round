object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 235
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 3
    Width = 21
    Height = 15
    Caption = 'MD5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 77
    Width = 28
    Height = 15
    Caption = 'SHA1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 157
    Width = 28
    Height = 15
    Caption = 'SHA2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object SHA1_edit: TEdit
    Left = 8
    Top = 98
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object sha1_button: TButton
    Left = 96
    Top = 125
    Width = 75
    Height = 25
    Caption = 'SHA 1'
    TabOrder = 1
    OnClick = sha1_buttonClick
  end
  object SHA1_result: TEdit
    Left = 135
    Top = 98
    Width = 370
    Height = 21
    TabOrder = 2
  end
  object SHA2_result: TEdit
    Left = 135
    Top = 178
    Width = 370
    Height = 21
    TabOrder = 3
  end
  object SHA2_edit: TEdit
    Left = 8
    Top = 178
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object SHA2_button: TButton
    Left = 96
    Top = 205
    Width = 75
    Height = 25
    Caption = 'SHA2'
    TabOrder = 5
    OnClick = sha2_buttonClick
  end
  object hash5_result: TEdit
    Left = 135
    Top = 24
    Width = 370
    Height = 21
    TabOrder = 6
  end
  object hash5_edit: TEdit
    Left = 8
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object hash5_button: TButton
    Left = 96
    Top = 51
    Width = 75
    Height = 25
    Caption = 'HASH 5'
    TabOrder = 8
    OnClick = hash5_buttonClick
  end
end
