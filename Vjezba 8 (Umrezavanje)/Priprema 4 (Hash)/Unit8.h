//---------------------------------------------------------------------------

#ifndef Unit8H
#define Unit8H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TEdit *SHA1_edit;
	TButton *sha1_button;
	TEdit *SHA1_result;
	TEdit *SHA2_result;
	TEdit *SHA2_edit;
	TButton *SHA2_button;
	TEdit *hash5_result;
	TEdit *hash5_edit;
	TButton *hash5_button;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
    void __fastcall hash5_buttonClick(TObject *Sender);
	void __fastcall sha1_buttonClick(TObject *Sender);
	void __fastcall sha2_buttonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
