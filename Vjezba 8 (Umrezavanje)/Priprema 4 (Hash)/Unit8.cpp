#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include <idhashmessagedigest.hpp>
#include <idhashsha.hpp>
#include <idsslopenssl.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner){
}

// MD HASH 5
void __fastcall Tmain_form::hash5_buttonClick(TObject *Sender) {
    TIdHashMessageDigest5 *md5 = new TIdHashMessageDigest5();
    hash5_result->Text = md5->HashStringAsHex(hash5_edit->Text);
    delete md5;
}

// SHA 1
void __fastcall Tmain_form::sha1_buttonClick(TObject *Sender) {
    TIdHashSHA1 *sha1 = new TIdHashSHA1();
    SHA1_result->Text = sha1->HashStringAsHex(SHA1_edit->Text);
    delete sha1;
}

// SHA 256
void __fastcall Tmain_form::sha2_buttonClick(TObject *Sender) {
    if(Idsslopenssl::LoadOpenSSLLibrary() != true) {
        ShowMessage("OpenSSL could not be loaded!");
        return;
	}
    TIdHashSHA256 *sha256 = new TIdHashSHA256;
    if(sha256->IsAvailable()) {
        SHA2_result->Text = sha256->HashStringAsHex(SHA2_edit->Text);
	} else {
        ShowMessage("SSL not available!");
	}
    delete sha256;
}
