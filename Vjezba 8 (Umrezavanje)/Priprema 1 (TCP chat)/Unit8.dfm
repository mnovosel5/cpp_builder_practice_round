object Form8: TForm8
  Left = 0
  Top = 0
  Caption = 'Form8'
  ClientHeight = 223
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object host_edit: TLabeledEdit
    Left = 8
    Top = 24
    Width = 305
    Height = 21
    EditLabel.Width = 28
    EditLabel.Height = 14
    EditLabel.Caption = 'host'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -12
    EditLabel.Font.Name = 'Consolas'
    EditLabel.Font.Style = []
    EditLabel.ParentFont = False
    TabOrder = 0
    Text = '127.0.0.1'
  end
  object chat_list_box: TListBox
    Left = 8
    Top = 51
    Width = 305
    Height = 134
    ItemHeight = 13
    TabOrder = 1
  end
  object chat_edit: TEdit
    Left = 8
    Top = 191
    Width = 224
    Height = 21
    TabOrder = 2
  end
  object send_button: TButton
    Left = 238
    Top = 191
    Width = 75
    Height = 26
    Caption = 'SEND'
    TabOrder = 3
    OnClick = send_buttonClick
  end
  object Panel1: TPanel
    Left = 319
    Top = 24
    Width = 66
    Height = 191
    TabOrder = 4
  end
  object tcp_client: TIdTCPClient
    ConnectTimeout = 0
    IPVersion = Id_IPv4
    Port = 12000
    ReadTimeout = -1
    Left = 336
    Top = 56
  end
  object tcp_server: TIdTCPServer
    Active = True
    Bindings = <
      item
        IP = '0.0.0.0'
        Port = 12000
      end>
    DefaultPort = 12000
    OnExecute = tcp_serverExecute
    Left = 336
    Top = 136
  end
end
