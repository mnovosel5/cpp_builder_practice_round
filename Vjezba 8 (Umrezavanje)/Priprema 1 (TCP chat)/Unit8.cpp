#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
TForm8 *Form8;

__fastcall TForm8::TForm8(TComponent* Owner): TForm(Owner) {
}

void __fastcall TForm8::send_buttonClick(TObject *Sender) {
    tcp_client->Host = host_edit->Text;
    tcp_client->Connect();
    // Write a message
    // tcp_client->Socket->Write(chat_edit->Text.Length());
    tcp_client->Socket->WriteLn(chat_edit->Text);
    tcp_client->Disconnect();
}

void __fastcall TForm8::tcp_serverExecute(TIdContext *AContext) {
    // int length = AContext->Connection->Socket->ReadLongInt();
    UnicodeString message = AContext->Connection->Socket->ReadLn();
    AContext->Connection->Disconnect();
    chat_list_box->Items->Add(message);
}
