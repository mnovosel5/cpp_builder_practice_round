//---------------------------------------------------------------------------

#ifndef Unit8H
#define Unit8H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdMessage.hpp>
#include <IdMessageClient.hpp>
#include <IdPOP3.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TIdPOP3 *pop3_client;
	TIdMessage *message_client;
	TPanel *Panel1;
	TButton *connect_button;
	TLabeledEdit *host_edit;
	TLabeledEdit *username_edit;
	TLabeledEdit *password_edit;
	TListBox *list_box;
	TLabel *Label1;
	TLabel *Label2;
	TMemo *memo_edit;
	void __fastcall connect_buttonClick(TObject *Sender);
	void __fastcall list_boxClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
