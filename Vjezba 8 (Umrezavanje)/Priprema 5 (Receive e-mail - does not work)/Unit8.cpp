#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include <strutils.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
}

void __fastcall Tmain_form::connect_buttonClick(TObject *Sender) {
    pop3_client->Host = host_edit->Text;
    pop3_client->Username = username_edit->Text;
    pop3_client->Password = password_edit->Text;

    pop3_client->Connect();
    int message_n = pop3_client->CheckMessages();
    for(int i = 0; i < message_n; i++) {
        pop3_client->Retrieve(i, message_client);
        list_box->Items->Add(message_client->Subject + "(" + message_client->From->Address + ")");
	}
    pop3_client->Disconnect();
}

void __fastcall Tmain_form::list_boxClick(TObject *Sender) {
    pop3_client->Connect();
    TIdText *text;
    pop3_client->Retrieve(list_box->ItemIndex + 1, message_client);
    for(int i = 0; i < message_client->MessageParts->Count; i++) {
        if((AnsiContainsStr(message_client->MessageParts->Items[i]->ContentType, "text/plain;"))) {
            text = dynamic_cast<TIdText*>(message_client->MessageParts->Items[i]);
            memo_edit->Lines->AddStrings(text->Body);
		}
	}
    pop3_client->Disconnect();
}
