object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 574
  ClientWidth = 336
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 167
    Width = 42
    Height = 15
    Caption = 'Emails'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 291
    Width = 70
    Height = 15
    Caption = 'Email text'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 193
    Height = 153
    Caption = 'Client data'
    TabOrder = 0
    object host_edit: TLabeledEdit
      Left = 16
      Top = 40
      Width = 161
      Height = 21
      EditLabel.Width = 22
      EditLabel.Height = 13
      EditLabel.Caption = 'Host'
      TabOrder = 0
    end
    object username_edit: TLabeledEdit
      Left = 16
      Top = 80
      Width = 161
      Height = 21
      EditLabel.Width = 48
      EditLabel.Height = 13
      EditLabel.Caption = 'Username'
      TabOrder = 1
    end
    object password_edit: TLabeledEdit
      Left = 16
      Top = 122
      Width = 161
      Height = 21
      EditLabel.Width = 46
      EditLabel.Height = 13
      EditLabel.Caption = 'Password'
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 208
    Top = 8
    Width = 121
    Height = 153
    TabOrder = 1
    object connect_button: TButton
      Left = 24
      Top = 120
      Width = 75
      Height = 25
      Caption = 'CONNECT'
      TabOrder = 0
      OnClick = connect_buttonClick
    end
  end
  object list_box: TListBox
    Left = 8
    Top = 188
    Width = 321
    Height = 97
    ItemHeight = 13
    TabOrder = 2
    OnClick = list_boxClick
  end
  object memo_edit: TMemo
    Left = 8
    Top = 312
    Width = 321
    Height = 257
    TabOrder = 3
  end
  object pop3_client: TIdPOP3
    AutoLogin = True
    SASLMechanisms = <>
    Left = 256
    Top = 24
  end
  object message_client: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 256
    Top = 80
  end
end
