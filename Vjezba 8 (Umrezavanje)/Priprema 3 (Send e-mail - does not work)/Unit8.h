//---------------------------------------------------------------------------

#ifndef Unit8H
#define Unit8H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdIOHandler.hpp>
#include <IdIOHandlerSocket.hpp>
#include <IdIOHandlerStack.hpp>
#include <IdMessage.hpp>
#include <IdMessageClient.hpp>
#include <IdSMTP.hpp>
#include <IdSMTPBase.hpp>
#include <IdSSL.hpp>
#include <IdSSLOpenSSL.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TGroupBox *GroupBox2;
	TGroupBox *GroupBox3;
	TListBox *status_list_box;
	TButton *add_button;
	TButton *remove_button;
	TButton *Send;
	TLabel *Status;
	TLabel *Body;
	TLabeledEdit *host_edit;
	TLabeledEdit *username_edit;
	TLabeledEdit *password_edit;
	TLabeledEdit *to_edit;
	TLabeledEdit *subject_edit;
	TListBox *attachment_list_box;
	TOpenDialog *open_dialog;
	TIdSMTP *smtp_client;
	TIdMessage *message_client;
	TIdSSLIOHandlerSocketOpenSSL *IdSSLIOHandlerSocketOpenSSL1;
	TMemo *memo_edit;
	void __fastcall SendClick(TObject *Sender);
	void __fastcall add_buttonClick(TObject *Sender);
	void __fastcall remove_buttonClick(TObject *Sender);
	void __fastcall IdSSLIOHandlerSocketOpenSSL1Status(TObject *ASender, const TIdStatus AStatus,
          const UnicodeString AStatusText);
	void __fastcall smtp_clientStatus(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText);


private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
