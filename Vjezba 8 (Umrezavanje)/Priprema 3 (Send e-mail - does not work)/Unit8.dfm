object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 457
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Status: TLabel
    Left = 199
    Top = 237
    Width = 31
    Height = 13
    Caption = 'Status'
  end
  object Body: TLabel
    Left = 199
    Top = 13
    Width = 24
    Height = 13
    Caption = 'Body'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 185
    Height = 153
    Caption = 'Login info'
    TabOrder = 0
    object host_edit: TLabeledEdit
      Left = 11
      Top = 40
      Width = 158
      Height = 21
      EditLabel.Width = 22
      EditLabel.Height = 13
      EditLabel.Caption = 'Host'
      TabOrder = 0
      Text = 'smtp.gmail.com'
    end
    object username_edit: TLabeledEdit
      Left = 11
      Top = 80
      Width = 158
      Height = 21
      EditLabel.Width = 48
      EditLabel.Height = 13
      EditLabel.Caption = 'Username'
      TabOrder = 1
      Text = 'mnovosel325'
    end
    object password_edit: TLabeledEdit
      Left = 11
      Top = 123
      Width = 158
      Height = 21
      EditLabel.Width = 46
      EditLabel.Height = 13
      EditLabel.Caption = 'Password'
      ParentShowHint = False
      PasswordChar = '*'
      ShowHint = False
      TabOrder = 2
      Text = 'filipbarisic3'
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 167
    Width = 185
    Height = 114
    Caption = 'Send info'
    TabOrder = 1
    object to_edit: TLabeledEdit
      Left = 11
      Top = 40
      Width = 158
      Height = 21
      EditLabel.Width = 12
      EditLabel.Height = 13
      EditLabel.Caption = 'To'
      TabOrder = 0
      Text = 'matija_novosel@net.hr'
    end
    object subject_edit: TLabeledEdit
      Left = 11
      Top = 80
      Width = 158
      Height = 21
      EditLabel.Width = 36
      EditLabel.Height = 13
      EditLabel.Caption = 'Subject'
      TabOrder = 1
      Text = 'TEST'
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 287
    Width = 185
    Height = 162
    Caption = 'Attachments'
    TabOrder = 2
    object add_button: TButton
      Left = 11
      Top = 127
      Width = 62
      Height = 25
      Caption = 'Add'
      TabOrder = 0
      OnClick = add_buttonClick
    end
    object remove_button: TButton
      Left = 106
      Top = 127
      Width = 63
      Height = 25
      Caption = 'Remove'
      TabOrder = 1
      OnClick = remove_buttonClick
    end
    object attachment_list_box: TListBox
      Left = 11
      Top = 24
      Width = 158
      Height = 97
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object status_list_box: TListBox
    Left = 199
    Top = 256
    Width = 290
    Height = 152
    ItemHeight = 13
    TabOrder = 3
  end
  object Send: TButton
    Left = 413
    Top = 414
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 4
    OnClick = SendClick
  end
  object memo_edit: TMemo
    Left = 199
    Top = 32
    Width = 289
    Height = 199
    TabOrder = 5
  end
  object open_dialog: TOpenDialog
    Left = 80
    Top = 343
  end
  object smtp_client: TIdSMTP
    OnStatus = smtp_clientStatus
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    SASLMechanisms = <>
    UseTLS = utUseRequireTLS
    Left = 288
    Top = 88
  end
  object message_client: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 376
    Top = 88
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    OnStatus = IdSSLIOHandlerSocketOpenSSL1Status
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 328
    Top = 152
  end
end
