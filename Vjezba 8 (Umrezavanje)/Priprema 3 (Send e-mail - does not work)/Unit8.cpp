#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include <IdMessageBuilder.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
}

void __fastcall Tmain_form::SendClick(TObject *Sender) {
    smtp_client->Host = host_edit->Text;
    smtp_client->Username = username_edit->Text;
    smtp_client->Password = password_edit->Text;

    message_client->Clear();
    message_client->Recipients->EMailAddresses = to_edit->Text;
    message_client->Subject = subject_edit->Text;
    message_client->Body->Text = memo_edit->Text;

    TIdMessageBuilderPlain *message = new TIdMessageBuilderPlain();
    if(attachment_list_box->Items->Count > 0) {
        for(int i = 0; i < attachment_list_box->Items->Count; i++) {
            message->Attachments->Add(attachment_list_box->Items->Strings[i]);
		}
        message->PlainText->Text = memo_edit->Text;
        message->FillMessage(message_client);
	}
    try {
        smtp_client->Connect();
        smtp_client->Send(message_client);
	} catch(...) {
        ShowMessage("Error!");
    }
    smtp_client->Disconnect();
    delete message;
}

void __fastcall Tmain_form::add_buttonClick(TObject *Sender) {
    if(open_dialog->Execute() == true) {
		attachment_list_box->Items->Add(open_dialog->FileName);
	}
}

void __fastcall Tmain_form::remove_buttonClick(TObject *Sender) {
    if(attachment_list_box->Items->Count == 0) return;
	attachment_list_box->Items->Delete(attachment_list_box->ItemIndex);
}

void __fastcall Tmain_form::IdSSLIOHandlerSocketOpenSSL1Status(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText) {
    status_list_box->Items->Add("OpenSSL: "  + AStatusText);
	Application->ProcessMessages();
}

void __fastcall Tmain_form::smtp_clientStatus(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText) {
    status_list_box->Items->Add("SMTP: " + AStatusText);
	Application->ProcessMessages();
}

