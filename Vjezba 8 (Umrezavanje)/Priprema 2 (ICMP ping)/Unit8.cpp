#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
}

void __fastcall Tmain_form::ping_buttonClick(TObject *Sender) {
    list_box->Items->Clear();
    icmp_client->Host = host_name->Text;
    for(int i = 0; i < 4; i++){
        try {
            icmp_client->Ping();
            Sleep(1000);
            Application->ProcessMessages();
        } catch(...) {
            list_box->Items->Add("Ping failed!");
            Application->ProcessMessages();
        }
    }
}

void __fastcall Tmain_form::icmp_clientReply(TComponent *ASender, TReplyStatus * const AReplyStatus) {
    UnicodeString result;
    result = "Received " + UnicodeString(AReplyStatus->BytesReceived) +
            " bytes from " + AReplyStatus->FromIpAddress +
            ", time = " + UnicodeString(AReplyStatus->MsRoundTripTime) +
            " ms, ttl = " + UnicodeString((int)AReplyStatus->TimeToLive);
    list_box->Items->Add(result);
}
