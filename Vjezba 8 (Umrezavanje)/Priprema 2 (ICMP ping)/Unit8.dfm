object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 142
  ClientWidth = 480
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object host_name: TEdit
    Left = 8
    Top = 8
    Width = 288
    Height = 21
    TabOrder = 0
    Text = 'www.google.com'
  end
  object list_box: TListBox
    Left = 8
    Top = 39
    Width = 369
    Height = 97
    ItemHeight = 13
    TabOrder = 1
  end
  object ping_button: TButton
    Left = 302
    Top = 8
    Width = 75
    Height = 25
    Caption = 'PING'
    TabOrder = 2
    OnClick = ping_buttonClick
  end
  object Panel1: TPanel
    Left = 383
    Top = 6
    Width = 89
    Height = 128
    TabOrder = 3
  end
  object icmp_client: TIdIcmpClient
    Protocol = 1
    ProtocolIPv6 = 58
    IPVersion = Id_IPv4
    OnReply = icmp_clientReply
    Left = 416
    Top = 56
  end
end
