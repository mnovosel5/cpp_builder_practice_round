//---------------------------------------------------------------------------

#ifndef Unit8H
#define Unit8H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdUDPBase.hpp>
#include <IdUDPClient.hpp>
#include <IdUDPServer.hpp>
#include <IdGlobal.hpp>
#include <IdSocketHandle.hpp>
#include <IdCustomTCPServer.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <IdTCPServer.hpp>
#include <IdContext.hpp>
#include <IdIcmpClient.hpp>
#include <IdRawBase.hpp>
#include <IdRawClient.hpp>
//---------------------------------------------------------------------------
class Tmain_form : public TForm
{
__published:	// IDE-managed Components
	TLabeledEdit *host_edit;
	TLabel *message_label;
	TEdit *message_edit;
	TButton *udp_send;
	TButton *udp_broadcast;
	TIdUDPClient *udp_client;
	TIdUDPServer *udp_server;
	TListBox *list_box;
	TIdTCPClient *tcp_client;
	TIdTCPServer *tcp_server;
	TButton *tcp_send;
	TButton *tcp_broadcast;
	TIdIcmpClient *icmp_client;
	TListBox *ping_box;
	TLabel *ping_label;
	TPanel *Panel1;
	TButton *ping_button;
	TLabeledEdit *ip_edit;
	TLabel *Label1;
	void __fastcall tcp_sendClick(TObject *Sender);
    void __fastcall udp_sendClick(TObject *Sender);
	void __fastcall udp_serverUDPRead(TIdUDPListenerThread *AThread, const TIdBytes AData,
          TIdSocketHandle *ABinding);
	void __fastcall udp_broadcastClick(TObject *Sender);
    void __fastcall tcp_broadcastClick(TObject *Sender);
	void __fastcall tcp_serverExecute(TIdContext *AContext);
	void __fastcall ping_buttonClick(TObject *Sender);
	void __fastcall icmp_clientReply(TComponent *ASender, TReplyStatus * const AReplyStatus);

private:	// User declarations
public:		// User declarations
	__fastcall Tmain_form(TComponent* Owner);
    HANDLE hMutex;
};
//---------------------------------------------------------------------------
extern PACKAGE Tmain_form *main_form;
//---------------------------------------------------------------------------
#endif
