object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'udp_client'
  ClientHeight = 335
  ClientWidth = 710
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object message_label: TLabel
    Left = 8
    Top = 249
    Width = 56
    Height = 15
    Caption = 'Message:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object ping_label: TLabel
    Left = 414
    Top = 46
    Width = 84
    Height = 15
    Caption = 'Ping results'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 8
    Top = 46
    Width = 28
    Height = 15
    Caption = 'Chat'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object host_edit: TLabeledEdit
    Left = 8
    Top = 19
    Width = 113
    Height = 21
    EditLabel.Width = 28
    EditLabel.Height = 15
    EditLabel.Caption = 'Host'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -13
    EditLabel.Font.Name = 'Consolas'
    EditLabel.Font.Style = []
    EditLabel.ParentFont = False
    TabOrder = 0
    Text = '127.0.0.1'
  end
  object message_edit: TEdit
    Left = 70
    Top = 247
    Width = 339
    Height = 21
    TabOrder = 1
  end
  object udp_send: TButton
    Left = 8
    Top = 274
    Width = 113
    Height = 25
    Caption = 'Send message (UDP)'
    TabOrder = 2
    OnClick = udp_sendClick
  end
  object udp_broadcast: TButton
    Left = 127
    Top = 274
    Width = 146
    Height = 25
    Caption = 'Broadcast message (UDP)'
    TabOrder = 3
    OnClick = udp_broadcastClick
  end
  object list_box: TListBox
    Left = 8
    Top = 67
    Width = 401
    Height = 174
    ItemHeight = 13
    TabOrder = 4
  end
  object tcp_send: TButton
    Left = 8
    Top = 305
    Width = 113
    Height = 25
    Caption = 'Send message (TCP)'
    TabOrder = 5
    OnClick = tcp_sendClick
  end
  object tcp_broadcast: TButton
    Left = 127
    Top = 305
    Width = 146
    Height = 25
    Caption = 'Broadcast message (TCP)'
    TabOrder = 6
    OnClick = tcp_broadcastClick
  end
  object ping_box: TListBox
    Left = 415
    Top = 67
    Width = 287
    Height = 199
    ItemHeight = 13
    TabOrder = 7
  end
  object Panel1: TPanel
    Left = 414
    Top = 274
    Width = 288
    Height = 56
    TabOrder = 8
  end
  object ping_button: TButton
    Left = 279
    Top = 274
    Width = 129
    Height = 56
    Caption = 'PING'
    TabOrder = 9
    OnClick = ping_buttonClick
  end
  object ip_edit: TLabeledEdit
    Left = 414
    Top = 19
    Width = 113
    Height = 21
    EditLabel.Width = 70
    EditLabel.Height = 15
    EditLabel.Caption = 'IP Address'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -13
    EditLabel.Font.Name = 'Consolas'
    EditLabel.Font.Style = []
    EditLabel.ParentFont = False
    TabOrder = 10
    Text = '127.0.0.1'
  end
  object udp_client: TIdUDPClient
    Active = True
    BoundIP = '127.0.0.3'
    BoundPort = 12222
    Host = '127.0.0.1'
    Port = 12222
    Left = 360
    Top = 136
  end
  object udp_server: TIdUDPServer
    Active = True
    Bindings = <
      item
        IP = '127.0.0.1'
        Port = 12222
      end>
    DefaultPort = 12222
    ThreadedEvent = True
    OnUDPRead = udp_serverUDPRead
    Left = 360
    Top = 80
  end
  object tcp_client: TIdTCPClient
    ConnectTimeout = 0
    IPVersion = Id_IPv4
    Port = 12222
    ReadTimeout = -1
    Left = 296
    Top = 136
  end
  object tcp_server: TIdTCPServer
    Active = True
    Bindings = <
      item
        IP = '127.0.0.1'
        Port = 12222
      end>
    DefaultPort = 12222
    OnExecute = tcp_serverExecute
    Left = 296
    Top = 80
  end
  object icmp_client: TIdIcmpClient
    Protocol = 1
    ProtocolIPv6 = 58
    IPVersion = Id_IPv4
    OnReply = icmp_clientReply
    Left = 232
    Top = 136
  end
end
