#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
}

void __fastcall Tmain_form::udp_sendClick(TObject *Sender) {
    udp_client->Host = host_edit->Text;
    udp_client->SendBuffer(host_edit->Text, 12222, ToBytes(message_edit->Text));
}

void __fastcall Tmain_form::udp_broadcastClick(TObject *Sender) {
    udp_client->Broadcast(message_edit->Text, 12222);
}

void __fastcall Tmain_form::udp_serverUDPRead(TIdUDPListenerThread *AThread, const TIdBytes AData, TIdSocketHandle *ABinding) {
    list_box->Items->Add(BytesToString(AData));
}

void __fastcall Tmain_form::tcp_broadcastClick(TObject *Sender) {
    tcp_client->Host = "0.0.0.0"; // Loopback address
    tcp_client->Connect();
    // Write a message
    tcp_client->Socket->Write(message_edit->Text.Length());
    tcp_client->Socket->Write(message_edit->Text);
    tcp_client->Disconnect();
}

void __fastcall Tmain_form::tcp_sendClick(TObject *Sender) {
    tcp_client->Host = host_edit->Text;
    tcp_client->Connect();
    // Write a message
    tcp_client->Socket->Write(message_edit->Text.Length());
    tcp_client->Socket->Write(message_edit->Text);
    tcp_client->Disconnect();
}

void __fastcall Tmain_form::tcp_serverExecute(TIdContext *AContext) {
    int length = AContext->Connection->Socket->ReadLongInt();
    UnicodeString message = AContext->Connection->Socket->ReadString(length);
    AContext->Connection->Disconnect();
    list_box->Items->Add(message);
}

void __fastcall Tmain_form::ping_buttonClick(TObject *Sender) {
	ping_box->Items->Clear();
    icmp_client->Host = ip_edit->Text;
    for(int i = 0; i < 4; i++){
        try {
            icmp_client->Ping();
            Sleep(1000);
            Application->ProcessMessages();
        } catch(...) {
            ping_box->Items->Add("Ping failed!");
            Application->ProcessMessages();
        }
    }
}

void __fastcall Tmain_form::icmp_clientReply(TComponent *ASender, TReplyStatus * const AReplyStatus) {
    UnicodeString result;
    result = "Received " + UnicodeString(AReplyStatus->BytesReceived) +
            " bytes from " + AReplyStatus->FromIpAddress +
            ", time = " + UnicodeString(AReplyStatus->MsRoundTripTime) +
            " ms, ttl = " + UnicodeString((int)AReplyStatus->TimeToLive);
    ping_box->Items->Add(result);
}

