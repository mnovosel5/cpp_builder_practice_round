object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 368
  ClientWidth = 582
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'New entry'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 89
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Edit entry'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 170
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Delete entry'
    TabOrder = 2
    OnClick = Button3Click
  end
  object DBGrid1: TDBGrid
    Left = 7
    Top = 39
    Width = 567
    Height = 194
    DataSource = datasource_subscriber
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Name'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Surname'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Email'
        Width = 107
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DateOfBirth'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Magazine'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Subscription'
        Width = 105
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 239
    Width = 185
    Height = 121
    Caption = 'Search columns'
    TabOrder = 4
    object ComboBox1: TComboBox
      Left = 11
      Top = 24
      Width = 158
      Height = 21
      TabOrder = 0
    end
    object LabeledEdit1: TLabeledEdit
      Left = 11
      Top = 64
      Width = 94
      Height = 21
      EditLabel.Width = 27
      EditLabel.Height = 13
      EditLabel.Caption = 'Data:'
      TabOrder = 1
    end
    object Button7: TButton
      Left = 111
      Top = 64
      Width = 58
      Height = 25
      Caption = 'Search'
      TabOrder = 2
      OnClick = Button7Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 199
    Top = 239
    Width = 185
    Height = 121
    Caption = 'Filter columns'
    TabOrder = 5
    object LabeledEdit2: TLabeledEdit
      Left = 16
      Top = 64
      Width = 89
      Height = 21
      EditLabel.Width = 27
      EditLabel.Height = 13
      EditLabel.Caption = 'Data:'
      TabOrder = 0
    end
    object ComboBox2: TComboBox
      Left = 16
      Top = 24
      Width = 161
      Height = 21
      TabOrder = 1
    end
    object CheckBox1: TCheckBox
      Left = 119
      Top = 63
      Width = 97
      Height = 17
      Caption = 'Filter'
      TabOrder = 2
      OnClick = CheckBox1Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 390
    Top = 239
    Width = 185
    Height = 121
    Caption = 'Sort by column'
    TabOrder = 6
    object Button4: TButton
      Left = 16
      Top = 24
      Width = 153
      Height = 25
      Caption = 'Name ASC'
      TabOrder = 0
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 16
      Top = 55
      Width = 153
      Height = 25
      Caption = 'Surname ASC'
      TabOrder = 1
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 16
      Top = 86
      Width = 153
      Height = 25
      Caption = 'Date of birth ASC'
      TabOrder = 2
      OnClick = Button6Click
    end
  end
  object ado_connection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\mnovosel2\' +
      'Desktop\repositories\cpp_builder_practice_round\vjezba_10 (ADO)\' +
      'Zadatak\database.mdb;Persist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 168
    Top = 152
  end
  object subscriber_table: TADOTable
    Active = True
    Connection = ado_connection
    CursorType = ctStatic
    TableName = 'Subscriber'
    Left = 264
    Top = 152
  end
  object datasource_subscriber: TDataSource
    DataSet = subscriber_table
    Left = 384
    Top = 152
  end
end
