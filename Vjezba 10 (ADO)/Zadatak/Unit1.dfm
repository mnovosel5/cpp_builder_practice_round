object entry_form: Tentry_form
  Left = 0
  Top = 0
  Caption = 'Add new entry'
  ClientHeight = 410
  ClientWidth = 286
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 270
    Height = 201
    Caption = 'Subscriber data'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 147
      Width = 61
      Height = 13
      Caption = 'Date of birth'
    end
    object LabeledEdit1: TLabeledEdit
      Left = 16
      Top = 40
      Width = 233
      Height = 21
      EditLabel.Width = 27
      EditLabel.Height = 13
      EditLabel.Caption = 'Name'
      TabOrder = 0
    end
    object LabeledEdit2: TLabeledEdit
      Left = 16
      Top = 80
      Width = 233
      Height = 21
      EditLabel.Width = 42
      EditLabel.Height = 13
      EditLabel.Caption = 'Surname'
      TabOrder = 1
    end
    object LabeledEdit3: TLabeledEdit
      Left = 16
      Top = 120
      Width = 233
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = 'Email'
      TabOrder = 2
    end
    object DateTimePicker1: TDateTimePicker
      Left = 16
      Top = 166
      Width = 233
      Height = 21
      Date = 43583.000000000000000000
      Time = 0.656466388885746700
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 215
    Width = 270
    Height = 58
    Caption = 'Magazine choice'
    TabOrder = 1
    object ComboBox1: TComboBox
      Left = 16
      Top = 24
      Width = 233
      Height = 21
      ItemIndex = 0
      TabOrder = 0
      Text = 'Magazine 1'
      Items.Strings = (
        'Magazine 1'
        'Magazine 2'
        'Magazine 3'
        'Magazine 4')
    end
  end
  object radio_group: TRadioGroup
    Left = 8
    Top = 279
    Width = 270
    Height = 124
    Caption = 'Subscription type'
    ItemIndex = 0
    Items.Strings = (
      '6 months'
      '12 months'
      '24 months')
    TabOrder = 2
  end
end
