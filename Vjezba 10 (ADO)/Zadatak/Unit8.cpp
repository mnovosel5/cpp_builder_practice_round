#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include "Unit1.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {
}

bool name_clicked = false;
bool surname_clicked = false;
bool date_clicked = false;

void __fastcall Tmain_form::Button1Click(TObject *Sender) {
    Tentry_form *entry_form = new Tentry_form(this);
    entry_form->ShowModal();

    UnicodeString name = entry_form->LabeledEdit1->Text;
    UnicodeString surname = entry_form->LabeledEdit2->Text;
    UnicodeString email = entry_form->LabeledEdit3->Text;
    TDate date_of_birth = entry_form->DateTimePicker1->Date;
    UnicodeString magazine = entry_form->ComboBox1->Items->Strings[ComboBox1->ItemIndex];
    UnicodeString subscription = entry_form->radio_group->Items->Strings[entry_form->radio_group->ItemIndex];

    subscriber_table->Insert();
    subscriber_table->FieldByName("Name")->AsString = name;
    subscriber_table->FieldByName("Surname")->AsString = surname;
    subscriber_table->FieldByName("Email")->AsString = email;
    subscriber_table->FieldByName("DateOfBirth")->AsDateTime = date_of_birth;
    subscriber_table->FieldByName("Magazine")->AsString = magazine;
    subscriber_table->FieldByName("Subscription")->AsString = subscription;
    subscriber_table->Post();

    delete entry_form;
}

void __fastcall Tmain_form::CheckBox1Click(TObject *Sender) {
    if(CheckBox1->Checked){
        if(LabeledEdit2->Text == "") {
            return;
		}
		subscriber_table->Filtered = true;
        // COLUMN LIKE %VALUE%
        subscriber_table->Filter = ComboBox2->Items->Strings[ComboBox2->ItemIndex] + " like '%" + LabeledEdit2->Text + "%'";
	} else {
		subscriber_table->Filtered = false;
	}
}

void __fastcall Tmain_form::Button7Click(TObject *Sender) {
    TLocateOptions options;
	options.Clear();
	options << loCaseInsensitive;
    if(!subscriber_table->Locate(ComboBox1->Items->Strings[ComboBox1->ItemIndex], LabeledEdit1->Text, options)) {
		ShowMessage("Data not found!");
	} else {
    	ShowMessage("Data has been found!");
	}
}

void __fastcall Tmain_form::FormCreate(TObject *Sender) {
    for(int i = 0; i < DBGrid1->Columns->Count; i++) {
        ComboBox1->Items->Add(DBGrid1->Columns->Items[i]->Title->Caption);
        ComboBox2->Items->Add(DBGrid1->Columns->Items[i]->Title->Caption);
	}
    ComboBox1->ItemIndex = 1;
	ComboBox2->ItemIndex = 1;
}

void __fastcall Tmain_form::Button2Click(TObject *Sender) {
    Tentry_form *entry_form = new Tentry_form(this);

    entry_form->Caption = "Edit entry";

    UnicodeString name = subscriber_table->FieldByName("Name")->AsString;
    UnicodeString surname =subscriber_table->FieldByName("Surname")->AsString;
    UnicodeString email = subscriber_table->FieldByName("Email")->AsString;
    TDate date_of_birth = subscriber_table->FieldByName("DateOfBirth")->AsDateTime;
    UnicodeString magazine = subscriber_table->FieldByName("Magazine")->AsString;
    UnicodeString subscription = subscriber_table->FieldByName("Subscription")->AsString;

    entry_form->LabeledEdit1->Text = name;
    entry_form->LabeledEdit2->Text = surname;
    entry_form->LabeledEdit3->Text = email;
    entry_form->DateTimePicker1->Date = date_of_birth;

    int index;

    for(int i = 0; i < entry_form->ComboBox1->Items->Count; i++) {
        if(entry_form->ComboBox1->Items->Strings[i] == magazine) {
            index = entry_form->ComboBox1->ItemIndex;
            break;
		}
	}

    entry_form->ComboBox1->ItemIndex = index;

    for(int i = 0; i < entry_form->radio_group->Items->Count; i++) {
        if(entry_form->radio_group->Items->Strings[i] == subscription) {
            index = entry_form->radio_group->ItemIndex;
            break;
		}
	}

    entry_form->radio_group->ItemIndex = index;

    entry_form->ShowModal();

    name = entry_form->LabeledEdit1->Text;
    surname = entry_form->LabeledEdit2->Text;
    email = entry_form->LabeledEdit3->Text;
    date_of_birth = entry_form->DateTimePicker1->Date;
    magazine = entry_form->ComboBox1->Items->Strings[ComboBox1->ItemIndex];
    subscription = entry_form->radio_group->Items->Strings[entry_form->radio_group->ItemIndex];

    // Start edit mode
    subscriber_table->Edit();

    subscriber_table->FieldByName("Name")->AsString = name;
    subscriber_table->FieldByName("Surname")->AsString = surname;
    subscriber_table->FieldByName("Email")->AsString = email;
    subscriber_table->FieldByName("DateOfBirth")->AsDateTime = date_of_birth;
    subscriber_table->FieldByName("Magazine")->AsString = magazine;
    subscriber_table->FieldByName("Subscription")->AsString = subscription;

	subscriber_table->Post();

    delete entry_form;
}

void __fastcall Tmain_form::Button3Click(TObject *Sender) {
    subscriber_table->Delete();
}

void __fastcall Tmain_form::Button4Click(TObject *Sender) {
    if(name_clicked) {
        Button4->Caption = "Name ASC";
        subscriber_table->IndexFieldNames = "Name ASC";
        name_clicked = false;
	} else {
        Button4->Caption = "Name DESC";
        subscriber_table->IndexFieldNames = "Name DESC";
        name_clicked = true;
	}
}

void __fastcall Tmain_form::Button5Click(TObject *Sender) {
    if(surname_clicked) {
        Button5->Caption = "Surname ASC";
        subscriber_table->IndexFieldNames = "Surname ASC";
        surname_clicked = false;
	} else {
        Button5->Caption = "Surname DESC";
        subscriber_table->IndexFieldNames = "Surname DESC";
        surname_clicked = true;
	}
}

void __fastcall Tmain_form::Button6Click(TObject *Sender) {
    if(date_clicked) {
        Button6->Caption = "DateOfBirth ASC";
        subscriber_table->IndexFieldNames = "DateOfBirth ASC";
        date_clicked = false;
	} else {
        Button6->Caption = "DateOfBirth DESC";
        subscriber_table->IndexFieldNames = "DateOfBirth DESC";
        date_clicked = true;
	}
}
