//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tentry_form : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit3;
	TDateTimePicker *DateTimePicker1;
	TLabel *Label1;
	TGroupBox *GroupBox2;
	TComboBox *ComboBox1;
	TRadioGroup *radio_group;
private:	// User declarations
public:		// User declarations
	__fastcall Tentry_form(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tentry_form *entry_form;
//---------------------------------------------------------------------------
#endif
