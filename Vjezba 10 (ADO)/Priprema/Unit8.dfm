object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 539
  ClientWidth = 580
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 357
    Width = 11
    Height = 13
    Caption = 'ID'
    FocusControl = DBEdit1
  end
  object Label2: TLabel
    Left = 8
    Top = 400
    Width = 33
    Height = 13
    Caption = 'Author'
    FocusControl = DBEdit2
  end
  object Label3: TLabel
    Left = 8
    Top = 446
    Width = 20
    Height = 13
    Caption = 'Title'
    FocusControl = DBEdit3
  end
  object Label4: TLabel
    Left = 8
    Top = 492
    Width = 56
    Height = 13
    Caption = 'PublishDate'
    FocusControl = DBEdit4
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 561
    Height = 153
    DataSource = datasource_book
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Author'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Title'
        Width = 145
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PublishDate'
        Width = 74
        Visible = True
      end>
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 167
    Width = 561
    Height = 153
    DataSource = datasource_borrow
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BookID'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Borrowed'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DateBorrowed'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DateReturned'
        Width = 75
        Visible = True
      end>
  end
  object DBEdit1: TDBEdit
    Left = 8
    Top = 376
    Width = 153
    Height = 21
    DataField = 'ID'
    DataSource = datasource_book
    TabOrder = 2
  end
  object DBEdit2: TDBEdit
    Left = 8
    Top = 419
    Width = 153
    Height = 21
    DataField = 'Author'
    DataSource = datasource_book
    TabOrder = 3
  end
  object DBEdit3: TDBEdit
    Left = 8
    Top = 465
    Width = 153
    Height = 21
    DataField = 'Title'
    DataSource = datasource_book
    TabOrder = 4
  end
  object DBEdit4: TDBEdit
    Left = 8
    Top = 511
    Width = 153
    Height = 21
    DataField = 'PublishDate'
    DataSource = datasource_book
    TabOrder = 5
  end
  object DBNavigator1: TDBNavigator
    Left = 8
    Top = 326
    Width = 240
    Height = 25
    DataSource = datasource_book
    TabOrder = 6
  end
  object Button1: TButton
    Left = 464
    Top = 326
    Width = 105
    Height = 25
    Caption = 'Insert new entry'
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 464
    Top = 357
    Width = 105
    Height = 25
    Caption = 'Delete an entry'
    TabOrder = 8
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 462
    Top = 415
    Width = 105
    Height = 25
    Caption = 'Locate entry'
    TabOrder = 9
    OnClick = Button3Click
  end
  object Edit1: TEdit
    Left = 464
    Top = 388
    Width = 103
    Height = 21
    TabOrder = 10
    Text = 'Edit1'
  end
  object ado_connection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\mnovosel2\' +
      'Desktop\repositories\cpp_builder_practice_round\vjezba_10 (ADO)\' +
      'Priprema\testing_database.mdb;Persist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 280
    Top = 432
  end
  object book_table: TADOTable
    Active = True
    Connection = ado_connection
    CursorType = ctStatic
    Filter = 'PublishDate = 1995'
    Filtered = True
    IndexFieldNames = 'ID'
    TableName = 'book'
    Left = 384
    Top = 328
    object book_tableID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object book_tableAuthor: TWideStringField
      FieldName = 'Author'
      Size = 255
    end
    object book_tableTitle: TWideStringField
      FieldName = 'Title'
      Size = 255
    end
    object book_tablePublishDate: TIntegerField
      FieldName = 'PublishDate'
    end
  end
  object borrow_table: TADOTable
    Active = True
    Connection = ado_connection
    CursorType = ctStatic
    IndexFieldNames = 'BookID'
    MasterFields = 'ID'
    MasterSource = datasource_book
    TableName = 'borrow'
    Left = 384
    Top = 376
  end
  object datasource_book: TDataSource
    DataSet = book_table
    Left = 280
    Top = 328
  end
  object datasource_borrow: TDataSource
    DataSet = borrow_table
    Left = 280
    Top = 376
  end
end
