#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner)
	: TForm(Owner)
{
}

void __fastcall Tmain_form::Button1Click(TObject *Sender) {
    book_table->Insert();
    book_table->FieldByName("Title")->AsString = "Nova knjiga";
    book_table->FieldByName("Author")->AsString = "Novi autor";
    book_table->FieldByName("PublishDate")->AsInteger = 2011;
    book_table->Post();
}

void __fastcall Tmain_form::Button2Click(TObject *Sender) {
    book_table->Delete();
}

void __fastcall Tmain_form::Button3Click(TObject *Sender) {
    TLocateOptions search_options;
    search_options.Clear();
    search_options << loPartialKey;
    if(book_table->Locate("PublishDate", Edit1->Text, search_options)) {
        ShowMessage("Found entry");
	} else {
        ShowMessage("Entry not found");
	}
}
