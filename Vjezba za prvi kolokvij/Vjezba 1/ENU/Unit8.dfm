object main_form: Tmain_form
  Left = 0
  Top = 0
  Caption = 'main_form'
  ClientHeight = 542
  ClientWidth = 1065
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 431
    Top = 243
    Width = 45
    Height = 13
    Caption = 'Hair color'
  end
  object something: TLabel
    Left = 431
    Top = 342
    Width = 345
    Height = 32
    Caption = 'THIS TEXT IS IN ENGLISH'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Consolas'
    Font.Pitch = fpFixed
    Font.Style = []
    Font.Quality = fqAntialiased
    ParentFont = False
  end
  object show_message_group: TGroupBox
    Left = 8
    Top = 8
    Width = 129
    Height = 137
    Caption = 'ShowMessage()'
    TabOrder = 0
    object show_message: TButton
      Left = 11
      Top = 24
      Width = 105
      Height = 25
      Caption = 'show_message'
      TabOrder = 0
      OnClick = show_messageClick
    end
  end
  object messagebox_group: TGroupBox
    Left = 143
    Top = 8
    Width = 138
    Height = 137
    Caption = 'MessageBox'
    TabOrder = 1
    object messagebox_0: TButton
      Left = 16
      Top = 24
      Width = 105
      Height = 25
      Caption = 'messagebox'
      TabOrder = 0
      OnClick = messagebox_0Click
    end
  end
  object task_dialog_group: TGroupBox
    Left = 287
    Top = 8
    Width = 138
    Height = 137
    Caption = 'TaskDialog'
    TabOrder = 2
    object task_dialog_button: TButton
      Left = 16
      Top = 24
      Width = 97
      Height = 25
      Caption = 'task_dialog'
      TabOrder = 0
      OnClick = task_dialog_buttonClick
    end
  end
  object messagedlg_group: TGroupBox
    Left = 431
    Top = 8
    Width = 138
    Height = 137
    Caption = 'MessageDlg'
    TabOrder = 3
    object message_dlg_button: TButton
      Left = 16
      Top = 24
      Width = 105
      Height = 25
      Caption = 'message_dialog'
      TabOrder = 0
      OnClick = message_dlg_buttonClick
    end
  end
  object input_box_group: TGroupBox
    Left = 8
    Top = 151
    Width = 129
    Height = 138
    Caption = 'InputBox'
    TabOrder = 4
    object inputbox_button: TButton
      Left = 11
      Top = 24
      Width = 105
      Height = 25
      Caption = 'input_box'
      TabOrder = 0
      OnClick = inputbox_buttonClick
    end
  end
  object inputquery_group: TGroupBox
    Left = 143
    Top = 151
    Width = 129
    Height = 138
    Caption = 'InputQuery'
    TabOrder = 5
    object inputquery_button: TButton
      Left = 11
      Top = 24
      Width = 105
      Height = 25
      Caption = 'input_query'
      TabOrder = 0
      OnClick = inputquery_buttonClick
    end
  end
  object taskdialog_input_group: TGroupBox
    Left = 278
    Top = 151
    Width = 129
    Height = 138
    Caption = 'TaskDialog (input)'
    TabOrder = 6
    object taskdialog_input_button: TButton
      Left = 11
      Top = 24
      Width = 105
      Height = 25
      Caption = 'task_dialog_input'
      TabOrder = 0
      OnClick = taskdialog_input_buttonClick
    end
  end
  object radio_group_0: TGroupBox
    Left = 8
    Top = 295
    Width = 129
    Height = 138
    Caption = 'Radio button group'
    TabOrder = 7
    object radio_button_0: TRadioButton
      Left = 13
      Top = 24
      Width = 113
      Height = 17
      Caption = 'Option 0'
      TabOrder = 0
    end
    object radio_button_1: TRadioButton
      Left = 13
      Top = 47
      Width = 113
      Height = 17
      Caption = 'Option 1'
      TabOrder = 1
    end
    object Button1: TButton
      Left = 13
      Top = 96
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object RadioButton3: TRadioButton
    Left = 159
    Top = 304
    Width = 113
    Height = 17
    Caption = 'RadioButton3'
    TabOrder = 8
  end
  object radio_group_1: TRadioGroup
    Left = 143
    Top = 327
    Width = 242
    Height = 138
    Caption = 'Tradio_group'
    Items.Strings = (
      'radio_0'
      'radio_1')
    TabOrder = 9
  end
  object Button2: TButton
    Left = 278
    Top = 391
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 10
    OnClick = Button2Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 431
    Top = 177
    Width = 145
    Height = 21
    EditLabel.Width = 61
    EditLabel.Height = 13
    EditLabel.Caption = 'LabeledEdit1'
    TabOrder = 11
  end
  object LabeledEdit2: TLabeledEdit
    Left = 431
    Top = 216
    Width = 145
    Height = 21
    EditLabel.Width = 61
    EditLabel.Height = 13
    EditLabel.Caption = 'LabeledEdit2'
    TabOrder = 12
  end
  object ComboBox1: TComboBox
    Left = 431
    Top = 259
    Width = 145
    Height = 21
    TabOrder = 13
    Text = 'ComboBox1'
  end
  object ListBox1: TListBox
    Left = 582
    Top = 177
    Width = 171
    Height = 103
    ItemHeight = 13
    TabOrder = 14
  end
  object Button3: TButton
    Left = 431
    Top = 300
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 15
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 431
    Top = 380
    Width = 75
    Height = 25
    Caption = 'CRO'
    TabOrder = 16
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 431
    Top = 411
    Width = 75
    Height = 25
    Caption = 'ENG'
    TabOrder = 17
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 8
    Top = 509
    Width = 75
    Height = 25
    Caption = 'REG save'
    TabOrder = 18
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 89
    Top = 509
    Width = 75
    Height = 25
    Caption = 'REG load'
    TabOrder = 19
    OnClick = Button7Click
  end
  object ListView1: TListView
    Left = 575
    Top = 8
    Width = 250
    Height = 150
    Columns = <
      item
        Caption = 'Author'
      end
      item
        Caption = 'Title'
      end>
    TabOrder = 20
    ViewStyle = vsReport
  end
  object Button8: TButton
    Left = 831
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Refresh XML'
    TabOrder = 21
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 831
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Add entry'
    TabOrder = 22
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 831
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Edit entry'
    TabOrder = 23
    OnClick = Button10Click
  end
  object Edit1: TEdit
    Left = 912
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 24
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 912
    Top = 39
    Width = 121
    Height = 21
    TabOrder = 25
    Text = 'Edit2'
  end
  object Button11: TButton
    Left = 831
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Delete entry'
    TabOrder = 26
    OnClick = Button11Click
  end
  object task_dialog: TTaskDialog
    Buttons = <>
    RadioButtons = <>
    Left = 336
    Top = 80
  end
  object xml_document: TXMLDocument
    Active = True
    FileName = 
      'C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_rou' +
      'nd\kolokvij_1 (xml, gui)\test.xml'
    NodeIndentStr = '    '
    Options = [doNodeAutoCreate, doNodeAutoIndent, doAttrNull, doAutoPrefix, doNamespaceDecl, doAutoSave]
    Left = 672
    Top = 64
  end
end
