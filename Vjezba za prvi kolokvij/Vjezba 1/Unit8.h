#ifndef Unit8H
#define Unit8H

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>

class Tmain_form: public TForm {
    __published:
	TGroupBox *show_message_group;
	TButton *show_message;
	TGroupBox *messagebox_group;
	TButton *messagebox_0;
	TTaskDialog *task_dialog;
	TGroupBox *task_dialog_group;
	TButton *task_dialog_button;
	TGroupBox *messagedlg_group;
	TButton *message_dlg_button;
	TGroupBox *input_box_group;
	TButton *inputbox_button;
	TGroupBox *inputquery_group;
	TButton *inputquery_button;
	TGroupBox *taskdialog_input_group;
	TButton *taskdialog_input_button;
	TGroupBox *radio_group_0;
	TRadioButton *radio_button_0;
	TRadioButton *radio_button_1;
	TRadioButton *RadioButton3;
	TRadioGroup *radio_group_1;
	TButton *Button1;
	TButton *Button2;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit2;
	TComboBox *ComboBox1;
	TListBox *ListBox1;
	TLabel *Label1;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TLabel *something;
	TButton *Button6;
	TButton *Button7;
	TListView *ListView1;
	TXMLDocument *xml_document;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TEdit *Edit1;
	TEdit *Edit2;
	TButton *Button11;
        void __fastcall show_messageClick(TObject *Sender);
	void __fastcall messagebox_0Click(TObject *Sender);
	void __fastcall task_dialog_buttonClick(TObject *Sender);
	void __fastcall message_dlg_buttonClick(TObject *Sender);
	void __fastcall inputbox_buttonClick(TObject *Sender);
	void __fastcall inputquery_buttonClick(TObject *Sender);
	void __fastcall taskdialog_input_buttonClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
    private:

    public:
        __fastcall Tmain_form(TComponent* Owner);
};

extern PACKAGE Tmain_form *main_form;

#endif
