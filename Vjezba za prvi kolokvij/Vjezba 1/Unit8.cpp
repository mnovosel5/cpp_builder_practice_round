#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include "Unit1.h"
#include "reinit.hpp"
#include "registry.hpp"
#include "test.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
Tmain_form *main_form;

__fastcall Tmain_form::Tmain_form(TComponent* Owner): TForm(Owner) {

}

void __fastcall Tmain_form::show_messageClick(TObject *Sender) {
    ShowMessage("mesage using ShowMessage() function");
}

void __fastcall Tmain_form::messagebox_0Click(TObject *Sender) {
    /*

        Application->MessageBox(poruka, naslov, gumbi | ikona | podrazumijevani_gumb);

        	poruka - Wide string
            naslov - Wide string
            gumbi -
                MB_OK
                MB_OKCANCEL
                MB_ABORTRETRYIGNORE
                MB_RETRYCANCEL
                MB_YESNO
                MB_YESNOCANCEL
                MB_HELP
            ikona -
                MB_ICONINFORMATION
                MB_ICONWARNING
                MB_ICONQUESTION
                MB_ICONSTOP
            podrazumijevani_gumb -
                MB_DEFBUTTON1
                MB_DEFBUTTON2
                MB_DEFBUTTON3
                MB_DEFBUTTON4

        Answer IDs:
            IDOK
            IDYES
            IDNO
            IDCANCEL
            IDRETRY
            IDIGNORE
            IDABORT
            IDCONTINUE
            IDTRYAGAIN

	*/
    int answer = Application->MessageBox(L"message", L"title", MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON2);
    if(answer == IDYES) {
        Application->MessageBox(L"you answered IDYES", L"outcome");
	} else {
        Application->MessageBox(L"you answered IDNO", L"outcome");
	}
}

void __fastcall Tmain_form::task_dialog_buttonClick(TObject *Sender) {
    // General information
	task_dialog->Caption = "caption";
    task_dialog->Title = "title";
    task_dialog->Text = "text";
    task_dialog->ExpandButtonCaption = "expand button caption";
    task_dialog->ExpandedText = "expanded text";
    task_dialog->FooterText = "footer text";
    task_dialog->FooterIcon = tdiWarning;
    // Buttons
    task_dialog->CommonButtons << tcbOk;
    TTaskDialogBaseButtonItem *button = task_dialog->Buttons->Add();
    button->Caption = "caption";
    button->ModalResult = mrCancel;
    task_dialog->Execute();
    if(task_dialog->ModalResult == mrOk) {
        ShowMessage("mrOk selected");
	} else {
        ShowMessage("mrOk not selected");
	}
}

void __fastcall Tmain_form::message_dlg_buttonClick(TObject *Sender) {
    MessageDlg(L"text", mtInformation, TMsgDlgButtons() << mbOK, 0);
}

void __fastcall Tmain_form::inputbox_buttonClick(TObject *Sender) {
    /*

        UnicodeString InputBox(UnicodeString title, UnicodeString message, UnicodeString default_value);

	*/
    UnicodeString result = InputBox("title", "message", "default_value");
    ShowMessage(result);
}

void __fastcall Tmain_form::inputquery_buttonClick(TObject *Sender) {
    /*

        void InputQuery("title", "message", UnicodeString &object_reference);

	*/
	UnicodeString result;
    if(InputQuery("title", "message", result)) {
        ShowMessage(result);
	} else {
        ShowMessage("cancel");
	}
}

void __fastcall Tmain_form::taskdialog_input_buttonClick(TObject *Sender){
    task_dialog->Title = "title";
    task_dialog->Caption = "caption";
    task_dialog->Text = "text";
    task_dialog->CommonButtons << tcbOk;

    TTaskDialogBaseButtonItem *radio_0 = task_dialog->RadioButtons->Add();
    TTaskDialogBaseButtonItem *radio_1 = task_dialog->RadioButtons->Add();
    TTaskDialogBaseButtonItem *radio_2 = task_dialog->RadioButtons->Add();
    TTaskDialogBaseButtonItem *radio_3 = task_dialog->RadioButtons->Add();

    radio_0->Caption = "radio_0";
    radio_1->Caption = "radio_1";
    radio_2->Caption = "radio_2";
    radio_3->Caption = "radio_3";

    task_dialog->VerificationText = "verification_text";
    task_dialog->Execute();

    ShowMessage(task_dialog->RadioButton->Index);
    if(task_dialog->Flags.Contains(tfVerificationFlagChecked)) {
        ShowMessage("verification_checked");
	}
}

void __fastcall Tmain_form::Button1Click(TObject *Sender) {
    TRadioButton *radio_buttons[2] {
        radio_button_0,
        radio_button_1
	};
    for(int i = 0; i < 2; i++) {
        if(radio_buttons[i]->Checked) {
            ShowMessage(radio_buttons[i]->Caption);
		}
	}
}

void __fastcall Tmain_form::Button2Click(TObject *Sender) {
	ShowMessage(radio_group_1->ItemIndex);
}

void __fastcall Tmain_form::Button3Click(TObject *Sender) {
    TForm1 *form = new TForm1(this);
    form->ShowModal();
    delete form;
}

void __fastcall Tmain_form::Button5Click(TObject *Sender) {
    const int ENGLISH = LANG_ENGLISH | (SUBLANG_ENGLISH_US << 10);
    if(LoadNewResourceModule(ENGLISH)) ReinitializeForms();
}

void __fastcall Tmain_form::Button4Click(TObject *Sender) {
    const int CROATIAN = LANG_CROATIAN | (SUBLANG_CROATIAN_CROATIA << 10);
    if(LoadNewResourceModule(CROATIAN)) ReinitializeForms();
}

void __fastcall Tmain_form::FormCreate(TObject *Sender) {
    ComboBox1->Items->Add("black");
    ComboBox1->Items->Add("green");
    ComboBox1->ItemIndex = 0;
    ListBox1->Items->Add("black");
    ListBox1->Items->Add("blue");
    TIniFile *ini = new TIniFile("settings.ini");
	this->Caption = ini->ReadString("main", "caption", 0);
    this->Top = ini->ReadInteger("main", "top", 0);
    this->Left = ini->ReadInteger("main", "left", 0);
    this->Width = ini->ReadInteger("main", "width", 0);
    this->Height = ini->ReadInteger("main", "height", 0);
}

void __fastcall Tmain_form::FormClose(TObject *Sender, TCloseAction &Action) {
    TIniFile *ini;
    ini = new TIniFile("settings.ini");
    ini->WriteString("main", "caption", this->Caption);
    ini->WriteInteger("main", "top", this->Top);
    ini->WriteInteger("main", "left", this->Left);
    ini->WriteInteger("main", "width", this->Width);
    ini->WriteInteger("main", "height", this->Height);
    delete ini;
}
void __fastcall Tmain_form::Button6Click(TObject *Sender) {
    TRegistry *registry = new TRegistry();
    registry->RootKey = HKEY_CURRENT_USER;
    UnicodeString key = "Software\\MatijaN";
    if(registry->OpenKey(key, true)) {
        registry->WriteInteger("top", this->Top);
        registry->WriteInteger("left", this->Left);
        registry->WriteInteger("width", this->Width);
        registry->WriteInteger("height", this->Height);
	}
    delete registry;
}

void __fastcall Tmain_form::Button7Click(TObject *Sender) {
    TRegistry *registry = new TRegistry();
    registry->RootKey = HKEY_CURRENT_USER;
    UnicodeString key = "Software\\MatijaN";
    if(registry->OpenKey(key, true)) {
        Top = registry->ReadInteger("top");
        Left = registry->ReadInteger("left");
        Width = registry->ReadInteger("width");
        Height = registry->ReadInteger("height");
    }
    delete registry;
}

void __fastcall Tmain_form::Button8Click(TObject *Sender) {
    _di_IXMLbookstoreType bookstore = Getbookstore(xml_document);
    ListView1->Items->Clear();
    for(int i = 0; i < bookstore->Count; i++) {
        ListView1->Items->Add();
        ListView1->Items->Item[i]->Caption = bookstore->book[i]->author;
        ListView1->Items->Item[i]->SubItems->Add(bookstore->book[i]->title);
	}
}

void __fastcall Tmain_form::Button9Click(TObject *Sender) {
    _di_IXMLbookstoreType bookstore = Getbookstore(xml_document);
    _di_IXMLbookType book = bookstore->Add();
    book->author = Edit1->Text;
    book->title = Edit2->Text;
    xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::Button11Click(TObject *Sender) {
    _di_IXMLbookstoreType bookstore = Getbookstore(xml_document);
    bookstore->Delete(ListView1->ItemIndex);
    xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall Tmain_form::Button10Click(TObject *Sender) {
	_di_IXMLbookstoreType bookstore = Getbookstore(xml_document);
    _di_IXMLbookType book = bookstore->book[ListView1->ItemIndex];
    book->author = Edit1->Text;
    book->title = Edit2->Text;
    xml_document->SaveToFile(xml_document->FileName);
}
