
// ************************************************************************************************************************************** //
//                                                                                                                                      
//                                                           XML Data Binding                                                           
//                                                                                                                                      
//         Generated on: 4/14/2019 10:00:02 PM                                                                                          
//       Generated from: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 2\people.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 1\test.xdb     
//                                                                                                                                      
// ************************************************************************************************************************************** //

#include <System.hpp>
#pragma hdrstop

#include "test.h"


// Global Functions 

_di_IXMLbookstoreType __fastcall Getbookstore(Xml::Xmlintf::_di_IXMLDocument Doc)
{
  return (_di_IXMLbookstoreType) Doc->GetDocBinding("bookstore", __classid(TXMLbookstoreType), TargetNamespace);
};

_di_IXMLbookstoreType __fastcall Getbookstore(Xml::Xmldoc::TXMLDocument *Doc)
{
  Xml::Xmlintf::_di_IXMLDocument DocIntf;
  Doc->GetInterface(DocIntf);
  return Getbookstore(DocIntf);
};

_di_IXMLbookstoreType __fastcall Loadbookstore(const System::UnicodeString& FileName)
{
  return (_di_IXMLbookstoreType) Xml::Xmldoc::LoadXMLDocument(FileName)->GetDocBinding("bookstore", __classid(TXMLbookstoreType), TargetNamespace);
};

_di_IXMLbookstoreType __fastcall  Newbookstore()
{
  return (_di_IXMLbookstoreType) Xml::Xmldoc::NewXMLDocument()->GetDocBinding("bookstore", __classid(TXMLbookstoreType), TargetNamespace);
};

// TXMLbookstoreType 

void __fastcall TXMLbookstoreType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("book"), __classid(TXMLbookType));
  ItemTag = "book";
  ItemInterface = __uuidof(IXMLbookType);
  Xml::Xmldoc::TXMLNodeCollection::AfterConstruction();
};

_di_IXMLbookType __fastcall TXMLbookstoreType::Get_book(int Index)
{
  return (_di_IXMLbookType) List->Nodes[Index];
};

_di_IXMLbookType __fastcall TXMLbookstoreType::Add()
{
  return (_di_IXMLbookType) AddItem(-1);
};

_di_IXMLbookType __fastcall TXMLbookstoreType::Insert(const int Index)
{
  return (_di_IXMLbookType) AddItem(Index);
};

// TXMLbookType 

System::UnicodeString __fastcall TXMLbookType::Get_title()
{
  return GetChildNodes()->Nodes[System::UnicodeString("title")]->Text;
};

void __fastcall TXMLbookType::Set_title(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("title")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLbookType::Get_author()
{
  return GetChildNodes()->Nodes[System::UnicodeString("author")]->Text;
};

void __fastcall TXMLbookType::Set_author(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("author")]->NodeValue = Value;
};
