
// ************************************************************************************************************************************** //
//                                                                                                                                      
//                                                           XML Data Binding                                                           
//                                                                                                                                      
//         Generated on: 4/14/2019 10:00:02 PM                                                                                          
//       Generated from: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 2\people.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 1\test.xdb     
//                                                                                                                                      
// ************************************************************************************************************************************** //

#ifndef   testH
#define   testH

#include <System.hpp>
#include <Xml.Xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.XMLDoc.hpp>
#include <XMLNodeImp.h>
#include <Xml.xmlutil.hpp>


// Forward Decls 

__interface IXMLbookstoreType;
typedef System::DelphiInterface<IXMLbookstoreType> _di_IXMLbookstoreType;
__interface IXMLbookType;
typedef System::DelphiInterface<IXMLbookType> _di_IXMLbookType;

// IXMLbookstoreType 

__interface INTERFACE_UUID("{E0C58290-53F9-4A63-8485-FE01F5F2CA64}") IXMLbookstoreType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual _di_IXMLbookType __fastcall Get_book(int Index) = 0;
  // Methods & Properties 
  virtual _di_IXMLbookType __fastcall Add() = 0;
  virtual _di_IXMLbookType __fastcall Insert(const int Index) = 0;
  __property _di_IXMLbookType book[int Index] = { read=Get_book };/* default */
};

// IXMLbookType 

__interface INTERFACE_UUID("{6825D28D-B4A7-4D13-8B08-EA74314EF34B}") IXMLbookType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_title() = 0;
  virtual System::UnicodeString __fastcall Get_author() = 0;
  virtual void __fastcall Set_title(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_author(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString title = { read=Get_title, write=Set_title };
  __property System::UnicodeString author = { read=Get_author, write=Set_author };
};

// Forward Decls 

class TXMLbookstoreType;
class TXMLbookType;

// TXMLbookstoreType 

class TXMLbookstoreType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLbookstoreType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLbookstoreType 
  virtual _di_IXMLbookType __fastcall Get_book(int Index);
  virtual _di_IXMLbookType __fastcall Add();
  virtual _di_IXMLbookType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLbookType 

class TXMLbookType : public Xml::Xmldoc::TXMLNode, public IXMLbookType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLbookType 
  virtual System::UnicodeString __fastcall Get_title();
  virtual System::UnicodeString __fastcall Get_author();
  virtual void __fastcall Set_title(System::UnicodeString Value);
  virtual void __fastcall Set_author(System::UnicodeString Value);
};

// Global Functions 

_di_IXMLbookstoreType __fastcall Getbookstore(Xml::Xmlintf::_di_IXMLDocument Doc);
_di_IXMLbookstoreType __fastcall Getbookstore(Xml::Xmldoc::TXMLDocument *Doc);
_di_IXMLbookstoreType __fastcall Loadbookstore(const System::UnicodeString& FileName);
_di_IXMLbookstoreType __fastcall  Newbookstore();

#define TargetNamespace ""

#endif