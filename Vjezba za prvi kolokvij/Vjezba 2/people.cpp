
// ************************************************************************************************************************************** //
//                                                                                                                                      
//                                                           XML Data Binding                                                           
//                                                                                                                                      
//         Generated on: 4/14/2019 9:39:47 AM                                                                                           
//       Generated from: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 2\people.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 2\people.xdb   
//                                                                                                                                      
// ************************************************************************************************************************************** //

#include <System.hpp>
#pragma hdrstop

#include "people.h"


// Global Functions 

_di_IXMLpeopleType __fastcall Getpeople(Xml::Xmlintf::_di_IXMLDocument Doc)
{
  return (_di_IXMLpeopleType) Doc->GetDocBinding("people", __classid(TXMLpeopleType), TargetNamespace);
};

_di_IXMLpeopleType __fastcall Getpeople(Xml::Xmldoc::TXMLDocument *Doc)
{
  Xml::Xmlintf::_di_IXMLDocument DocIntf;
  Doc->GetInterface(DocIntf);
  return Getpeople(DocIntf);
};

_di_IXMLpeopleType __fastcall Loadpeople(const System::UnicodeString& FileName)
{
  return (_di_IXMLpeopleType) Xml::Xmldoc::LoadXMLDocument(FileName)->GetDocBinding("people", __classid(TXMLpeopleType), TargetNamespace);
};

_di_IXMLpeopleType __fastcall  Newpeople()
{
  return (_di_IXMLpeopleType) Xml::Xmldoc::NewXMLDocument()->GetDocBinding("people", __classid(TXMLpeopleType), TargetNamespace);
};

// TXMLpeopleType 

void __fastcall TXMLpeopleType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("person"), __classid(TXMLpersonType));
  ItemTag = "person";
  ItemInterface = __uuidof(IXMLpersonType);
  Xml::Xmldoc::TXMLNodeCollection::AfterConstruction();
};

_di_IXMLpersonType __fastcall TXMLpeopleType::Get_person(int Index)
{
  return (_di_IXMLpersonType) List->Nodes[Index];
};

_di_IXMLpersonType __fastcall TXMLpeopleType::Add()
{
  return (_di_IXMLpersonType) AddItem(-1);
};

_di_IXMLpersonType __fastcall TXMLpeopleType::Insert(const int Index)
{
  return (_di_IXMLpersonType) AddItem(Index);
};

// TXMLpersonType 

System::UnicodeString __fastcall TXMLpersonType::Get_name()
{
  return GetChildNodes()->Nodes[System::UnicodeString("name")]->Text;
};

void __fastcall TXMLpersonType::Set_name(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("name")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLpersonType::Get_surname()
{
  return GetChildNodes()->Nodes[System::UnicodeString("surname")]->Text;
};

void __fastcall TXMLpersonType::Set_surname(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("surname")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLpersonType::Get_jmbag()
{
  return GetChildNodes()->Nodes[System::UnicodeString("jmbag")]->Text;
};

void __fastcall TXMLpersonType::Set_jmbag(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("jmbag")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLpersonType::Get_eye_color()
{
  return GetChildNodes()->Nodes[System::UnicodeString("eye_color")]->Text;
};

void __fastcall TXMLpersonType::Set_eye_color(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("eye_color")]->NodeValue = Value;
};
