#include <vcl.h>
#pragma hdrstop

#include "Unit8.h"
#include "people.h"
#include "reinit.hpp"
#include "registry.hpp"

#pragma package(smart_init)
#pragma resource "*.dfm"
TForm8 *Form8;

__fastcall TForm8::TForm8(TComponent* Owner): TForm(Owner) {
}

void __fastcall TForm8::Button1Click(TObject *Sender) {
    int result = Application->MessageBox(L"Are you sure you want to refresh?", L"MessageBox", MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON1);
    if(result == IDYES) {
        ListView1->Items->Clear();
        // Refresh XML, that is - LOAD XML
        _di_IXMLpeopleType people = Getpeople(xml_document);
        for(int i = 0; i < people->Count; i++) {
            ListView1->Items->Add();
            ListView1->Items->Item[i]->Caption = people->person[i]->name;
            ListView1->Items->Item[i]->SubItems->Add(people->person[i]->surname);
            ListView1->Items->Item[i]->SubItems->Add(people->person[i]->jmbag);
            ListView1->Items->Item[i]->SubItems->Add(people->person[i]->eye_color);
        }
	}
}

void __fastcall TForm8::FormCreate(TObject *Sender) {
    OpenDialog1->Filter = "Text files (*.txt)|*.TXT|All files(*)|*";
    if(OpenDialog1->Execute()) {
        ShowMessage(OpenDialog1->FileName);
	} else {
        ShowMessage("Nothing");
	}
    // Combo box items
	ComboBox1->Items->Add("green");
    ComboBox1->Items->Add("red");
    ComboBox1->Items->Add("cyan");
    ComboBox1->ItemIndex = 0;
}

void __fastcall TForm8::Button2Click(TObject *Sender) {
    // Add XML entry
    _di_IXMLpeopleType people = Getpeople(xml_document);
    _di_IXMLpersonType person = people->Add();
    person->name = Edit1->Text;
    person->surname = Edit2->Text;
    person->jmbag = Edit3->Text;
    person->eye_color = ComboBox1->Text;
    xml_document->SaveToFile(xml_document->FileName);
}

void __fastcall TForm8::Button3Click(TObject *Sender) {
    // Delete XML entry
    _di_IXMLpeopleType people = Getpeople(xml_document);
    if(ListView1->ItemIndex >= 0) {
        int index = ListView1->ItemIndex;
        people->Delete(index);
        xml_document->SaveToFile(xml_document->FileName);
	}
}

void __fastcall TForm8::Button4Click(TObject *Sender) {
    // Update XML entry
    _di_IXMLpeopleType people = Getpeople(xml_document);
    if(ListView1->ItemIndex >= 0) {
        people->person[ListView1->ItemIndex]->name = Edit1->Text;
        people->person[ListView1->ItemIndex]->surname = Edit2->Text;
        people->person[ListView1->ItemIndex]->jmbag = Edit3->Text;
        people->person[ListView1->ItemIndex]->eye_color = ComboBox1->Text;
        xml_document->SaveToFile(xml_document->FileName);
	}
}

void __fastcall TForm8::Button5Click(TObject *Sender) {
    // Save INI
    TIniFile *ini = new TIniFile("options.ini");
    ini->WriteInteger("main", "top", Top);
    ini->WriteInteger("main", "left", Left);
    ini->WriteInteger("main", "width", Width);
    ini->WriteInteger("main", "height", Height);
    delete ini;
}

void __fastcall TForm8::Button6Click(TObject *Sender) {
    // Load INI
	TIniFile *ini = new TIniFile("options.ini");
    Top = ini->ReadInteger("main", "top", 0);
    Left = ini->ReadInteger("main", "left", 0);
    Width = ini->ReadInteger("main", "width", 0);
    Height = ini->ReadInteger("main", "height", 0);
    delete ini;
}

void __fastcall TForm8::Button7Click(TObject *Sender) {
    // Save REG
	TRegistry *registry = new TRegistry();
    registry->RootKey = HKEY_CURRENT_USER;
    UnicodeString key = "Software\\kolokvij_1";
    // If the key does not exist
    if(!registry->KeyExists(key)) registry->CreateKey(key);
    if(registry->OpenKey(key, true)) {
        registry->WriteInteger("top", Top);
        registry->WriteInteger("left", Left);
        registry->WriteInteger("width", Width);
        registry->WriteInteger("height", Height);
        registry->CloseKey();
	}
    delete registry;
}

void __fastcall TForm8::Button8Click(TObject *Sender) {
    // Load REG
	TRegistry *registry = new TRegistry();
    registry->RootKey = HKEY_CURRENT_USER;
    UnicodeString key = "Software\\kolokvij_1";
    if(registry->KeyExists(key)) {
        if(registry->OpenKey(key, true)) {
            Top = registry->ReadInteger("top");
            Left = registry->ReadInteger("left");
            Width = registry->ReadInteger("width");
            Height = registry->ReadInteger("height");
        }
        registry->CloseKey();
	}
    delete registry;
}

void __fastcall TForm8::Button10Click(TObject *Sender) {
    const int CROATIAN = LANG_CROATIAN | (SUBLANG_CROATIAN_CROATIA << 10);
    if(LoadNewResourceModule(CROATIAN)) ReinitializeForms();
}

void __fastcall TForm8::Button9Click(TObject *Sender) {
    const int ENGLISH = LANG_ENGLISH | (SUBLANG_ENGLISH_US << 10);
    if(LoadNewResourceModule(ENGLISH)) ReinitializeForms();
}

void __fastcall TForm8::Button11Click(TObject *Sender) {
    ListView1->Items->Item[0]->Caption = "Something";
}
