
// ************************************************************************************************************************************** //
//                                                                                                                                      
//                                                           XML Data Binding                                                           
//                                                                                                                                      
//         Generated on: 4/14/2019 9:39:47 AM                                                                                           
//       Generated from: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 2\people.xml   
//   Settings stored in: C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_round\kolokvij_1 (xml, gui)\Vjezba 2\people.xdb   
//                                                                                                                                      
// ************************************************************************************************************************************** //

#ifndef   peopleH
#define   peopleH

#include <System.hpp>
#include <Xml.Xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.XMLDoc.hpp>
#include <XMLNodeImp.h>
#include <Xml.xmlutil.hpp>


// Forward Decls 

__interface IXMLpeopleType;
typedef System::DelphiInterface<IXMLpeopleType> _di_IXMLpeopleType;
__interface IXMLpersonType;
typedef System::DelphiInterface<IXMLpersonType> _di_IXMLpersonType;

// IXMLpeopleType 

__interface INTERFACE_UUID("{5721B93A-BE81-4C7D-A673-DF7CFC530780}") IXMLpeopleType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual _di_IXMLpersonType __fastcall Get_person(int Index) = 0;
  // Methods & Properties 
  virtual _di_IXMLpersonType __fastcall Add() = 0;
  virtual _di_IXMLpersonType __fastcall Insert(const int Index) = 0;
  __property _di_IXMLpersonType person[int Index] = { read=Get_person };/* default */
};

// IXMLpersonType 

__interface INTERFACE_UUID("{E312DAF9-D0EB-433D-B94F-C676D75E9AAB}") IXMLpersonType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_name() = 0;
  virtual System::UnicodeString __fastcall Get_surname() = 0;
  virtual System::UnicodeString __fastcall Get_jmbag() = 0;
  virtual System::UnicodeString __fastcall Get_eye_color() = 0;
  virtual void __fastcall Set_name(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_surname(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_jmbag(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_eye_color(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString name = { read=Get_name, write=Set_name };
  __property System::UnicodeString surname = { read=Get_surname, write=Set_surname };
  __property System::UnicodeString jmbag = { read=Get_jmbag, write=Set_jmbag };
  __property System::UnicodeString eye_color = { read=Get_eye_color, write=Set_eye_color };
};

// Forward Decls 

class TXMLpeopleType;
class TXMLpersonType;

// TXMLpeopleType 

class TXMLpeopleType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLpeopleType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLpeopleType 
  virtual _di_IXMLpersonType __fastcall Get_person(int Index);
  virtual _di_IXMLpersonType __fastcall Add();
  virtual _di_IXMLpersonType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLpersonType 

class TXMLpersonType : public Xml::Xmldoc::TXMLNode, public IXMLpersonType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLpersonType 
  virtual System::UnicodeString __fastcall Get_name();
  virtual System::UnicodeString __fastcall Get_surname();
  virtual System::UnicodeString __fastcall Get_jmbag();
  virtual System::UnicodeString __fastcall Get_eye_color();
  virtual void __fastcall Set_name(System::UnicodeString Value);
  virtual void __fastcall Set_surname(System::UnicodeString Value);
  virtual void __fastcall Set_jmbag(System::UnicodeString Value);
  virtual void __fastcall Set_eye_color(System::UnicodeString Value);
};

// Global Functions 

_di_IXMLpeopleType __fastcall Getpeople(Xml::Xmlintf::_di_IXMLDocument Doc);
_di_IXMLpeopleType __fastcall Getpeople(Xml::Xmldoc::TXMLDocument *Doc);
_di_IXMLpeopleType __fastcall Loadpeople(const System::UnicodeString& FileName);
_di_IXMLpeopleType __fastcall  Newpeople();

#define TargetNamespace ""

#endif