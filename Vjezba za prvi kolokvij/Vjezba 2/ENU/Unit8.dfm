object Form8: TForm8
  Left = 0
  Top = 0
  Caption = 'Form8'
  ClientHeight = 368
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListView1: TListView
    Left = 8
    Top = 8
    Width = 318
    Height = 321
    Columns = <
      item
        Caption = 'Name'
      end
      item
        Caption = 'Surname'
      end
      item
        Caption = 'JMBAG'
      end
      item
        Caption = 'Eye color'
      end>
    TabOrder = 0
    ViewStyle = vsReport
  end
  object Button1: TButton
    Left = 8
    Top = 335
    Width = 75
    Height = 25
    Caption = 'Refresh XML'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 89
    Top = 335
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 170
    Top = 335
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 251
    Top = 335
    Width = 75
    Height = 25
    Caption = 'Update'
    TabOrder = 4
    OnClick = Button4Click
  end
  object GroupBox1: TGroupBox
    Left = 332
    Top = 8
    Width = 237
    Height = 137
    Caption = 'GroupBox1'
    TabOrder = 5
    object Edit1: TEdit
      Left = 16
      Top = 24
      Width = 209
      Height = 21
      TabOrder = 0
      Text = 'Edit1'
    end
    object Edit2: TEdit
      Left = 16
      Top = 51
      Width = 209
      Height = 21
      TabOrder = 1
      Text = 'Edit2'
    end
    object Edit3: TEdit
      Left = 16
      Top = 78
      Width = 209
      Height = 21
      TabOrder = 2
      Text = 'Edit3'
    end
    object ComboBox1: TComboBox
      Left = 16
      Top = 105
      Width = 209
      Height = 21
      TabOrder = 3
      Text = 'ComboBox1'
    end
  end
  object Button5: TButton
    Left = 372
    Top = 151
    Width = 75
    Height = 25
    Caption = 'Save .ini'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 453
    Top = 151
    Width = 75
    Height = 25
    Caption = 'Load .ini'
    TabOrder = 7
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 372
    Top = 182
    Width = 75
    Height = 25
    Caption = 'Save REG'
    TabOrder = 8
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 453
    Top = 182
    Width = 75
    Height = 25
    Caption = 'Load REG'
    TabOrder = 9
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 453
    Top = 213
    Width = 75
    Height = 25
    Caption = 'ENG'
    TabOrder = 10
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 372
    Top = 213
    Width = 75
    Height = 25
    Caption = 'CRO'
    TabOrder = 11
    OnClick = Button10Click
  end
  object xml_document: TXMLDocument
    Active = True
    FileName = 
      'C:\Users\mnovosel2\Desktop\repositories\cpp_builder_practice_rou' +
      'nd\kolokvij_1 (xml, gui)\Vjezba 2\people.xml'
    NodeIndentStr = '    '
    Options = [doNodeAutoCreate, doNodeAutoIndent, doAttrNull, doAutoPrefix, doNamespaceDecl]
    Left = 152
    Top = 160
  end
end
