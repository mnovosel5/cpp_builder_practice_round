#pragma hdrstop
#pragma argsused

#include "windows.h"
#include "main.h"

// Function pointers
double sum(double a, double b) {
    return(a + b);
}

extern "C" {
    // Create a new instance implementation
    DLL_EXPORT Dynamic* allocator() {
        return(new Dynamic());
    }
    // Deleting an instance implementation
    DLL_EXPORT void deleter(Dynamic *obj) {
        delete(obj);
    }
}

// Setters
void Dynamic::set_a(int a) {
    this->a = a;
}

void Dynamic::set_b(char b) {
    this->b = b;
}

void Dynamic::set_c(float c) {
    this->c = c;
}

// Getters
int Dynamic::get_a() {
    return(this->a);
}

char Dynamic::get_b() {
    return(this->b);
}

float Dynamic::get_c() {
    return(this->c);
}

extern "C" {
    DLL_EXPORT GET_A get_get_a() {
    	return(&Dynamic::get_a);
	}
    DLL_EXPORT SET_A get_set_a() {
    	return(&Dynamic::set_a);
	}
}
