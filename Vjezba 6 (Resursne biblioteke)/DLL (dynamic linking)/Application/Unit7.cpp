#include <vcl.h>
#pragma hdrstop

#include "Unit7.h"
#include "windows.h"
// Include header file for the .dll code
#include "main.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

// Name of the dynamic-link library.
const wchar_t *library = L"Project1.dll";

TForm7 *Form7;

__fastcall TForm7::TForm7(TComponent* Owner): TForm(Owner) {

}

void __fastcall TForm7::Button1Click(TObject *Sender) {
	HINSTANCE test_dll = LoadLibrary(library);
    // Sum function
    p_sum sum = (p_sum)GetProcAddress(test_dll, "sum");

    ALLOCATOR create_class;
    DELETER delete_class;

    // get_a()
    GET_GET_A get_get_a; // Get method pointer
    GET_A get_a;

    // set_a()
    GET_SET_A get_set_a;
    SET_A set_a;

    Dynamic *dynamic;

    create_class = (ALLOCATOR)GetProcAddress(test_dll, "allocator");
    delete_class = (DELETER)GetProcAddress(test_dll, "deleter");
    get_get_a = (GET_GET_A)GetProcAddress(test_dll, "get_get_a");
    get_set_a = (GET_SET_A)GetProcAddress(test_dll, "get_set_a");

	// Test if the connection was successful
    if(!test_dll) {
        Application->MessageBox(L"Cannot find .dll!", L"Error", 0);
        return;
	}

    // Function pointer sum is addressed to the function in the .dll file
    if(!sum) {
        Application->MessageBox(L"No such function in the .dll file!", L"Error", 0);
        return;
	}

    // Calling the sum function
    ShowMessage(sum(1, 2));

    // Class instance
    dynamic = create_class();

    // Setter
    set_a = get_set_a();
	(dynamic->*set_a)(5);

    // Getter
    get_a = get_get_a();
    int n = (dynamic->*get_a)();

    ShowMessage(n);

    // Deallocate class
    delete_class(dynamic);

    FreeLibrary(test_dll);
}
