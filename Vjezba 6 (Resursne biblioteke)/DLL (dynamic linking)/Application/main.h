#define DLL_EXPORT __declspec(dllexport) __stdcall
#define EXPORT __declspec(dllexport)

extern "C" {
	double DLL_EXPORT sum(double a, double b);
    class EXPORT Dynamic {
        private:
            int a;
            char b;
            float c;
        public:
            void set_a(int);
            void set_b(char);
            void set_c(float);
            int get_a();
            char get_b();
            float get_c();
    };
}

// Sum function
typedef double (*__stdcall p_sum)(double, double);

// Dynamic class method pointers
typedef Dynamic* (*__stdcall ALLOCATOR)(); // Create a new instance
typedef void (*__stdcall DELETER)(Dynamic*); // Delete an instance

// get_a()
typedef int (Dynamic::*GET_A)();
typedef GET_A (*__stdcall GET_GET_A)();

// set_a()
typedef void (Dynamic::*SET_A)(int);
typedef SET_A (*__stdcall GET_SET_A)();
