#include <vcl.h>
#pragma hdrstop

#include "Unit7.h"
// Include header file for the .dll code
#include "main.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

TForm7 *Form7;

__fastcall TForm7::TForm7(TComponent* Owner): TForm(Owner) {

}

void __fastcall TForm7::Button1Click(TObject *Sender) {
    // Calling the sum function
    ShowMessage(sum(1, 2));
    // Instancing the Dynamic class
    Dynamic *dynamic = new Dynamic();
    dynamic->set_a(5);
    dynamic->set_b('C');
    ShowMessage(dynamic->get_a());
}
