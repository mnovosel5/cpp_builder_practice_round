#pragma hdrstop
#pragma argsused
#include "main.h"

double sum(double a, double b) {
    return(a + b);
}

// Setters
void Dynamic::set_a(int a) {
    this->a  = a;
}

void Dynamic::set_b(char b) {
    this->b  = b;
}

void Dynamic::set_c(float c) {
    this->c  = c;
}

// Getters
int Dynamic::get_a() {
    return(this->a);
}

char Dynamic::get_b() {
    return(this->b);
}

float Dynamic::get_c() {
    return(this->c);
}
