extern "C" {
    double __declspec(dllexport) __stdcall sum(double a, double b);
    class __declspec(dllexport) Dynamic {
        private:
            int a;
            char b;
            float c;
        public:
            void __stdcall set_a(int);
            void __stdcall set_b(char);
            void __stdcall set_c(float);
            int __stdcall get_a();
            char __stdcall get_b();
            float __stdcall get_c();
    };
}
