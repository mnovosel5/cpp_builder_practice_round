﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace testing_dll {
	public partial class Form1 : Form {
		[DllImport("Project1.dll")]
		private static extern double sum(double a, double b);
		public Form1() {
			InitializeComponent();
		}
		private void button1_Click(object sender, EventArgs e) {
			MessageBox.Show(sum(1, 2).ToString());
		}
	}
}
