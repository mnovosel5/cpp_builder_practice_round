#include "main.h"

void Static::set_a(int a){
    this->a = a;
}

void Static::set_b(char b){
    this->b = b;
}

void Static::set_c(float c){
    this->c = c;
}

int Static::get_a() {
    return(a);
}

char Static::get_b() {
    return(b);
}

float Static::get_c() {
    return(c);
}

double sum(double a, double b) {
    return(a + b);
}
