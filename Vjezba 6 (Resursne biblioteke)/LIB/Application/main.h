class Static {
    private:
        int a;
        char b;
        float c;
    public:
        void set_a(int a);
        void set_b(char b);
        void set_c(float c);
        int get_a();
        char get_b();
        float get_c();

};

double sum(double a, double);
